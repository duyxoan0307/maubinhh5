package com.mygdx.catte.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.BotController.BotBasicController;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.IZen;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.MyLinkedMap;
import com.mygdx.catte.View.ListCardView;

import java.util.Dictionary;
import java.util.Map;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayerModel {

    public InfoModel infoModel;
    public int index;
    GameModel gameModel;
    public Array<Array<CardModel>> listChi;
    BotBasicController botController;
    public boolean isCompletedSort;
    public PlayerModel(GameModel gameModel){
        listChi=new Array<Array<CardModel>>();
        for (int i=0;i<3;i++)
        {
            listChi.add(new Array<CardModel>());
        }
        this.gameModel=gameModel;
        listResultWithPlayer = new MyLinkedMap<PlayerModel, Integer>();
        listScoreWithPlayer = new MyLinkedMap<PlayerModel, Integer>();
        listScoreWithPlayerOfBranch = new MyLinkedMap<PlayerModel, Integer>();
        listResult=new Array<Integer>();
    }
    public void SetBotController(BotBasicController botBasicController){
        this.botController=botBasicController;
    }
    public void AddNewCard(CardModel cardModel){
        int index_chi;

        if (listChi.get(0).size<5)
            index_chi=0;
        else
            if (listChi.get(1).size<5)
                index_chi=1;
        else
                index_chi=2;
        listChi.get(index_chi).add(cardModel);
        //System.out.println("Player "+index+","+"Chi "+index_chi+"," + listChi[index_chi].size);
        SetUnChildToSort(index_chi);
        for (CardModel card:listChi.get(index_chi))
            card.SetParent(index,index_chi);
    }

    public void SetUpdateListHold(){
        for (int i=0;i<3;i++)
        {
            SetUnChildToSort(i);
            for (CardModel card:listChi.get(i))
                card.SetParent(index,i);
            SortList(i);
        }
        SetTypeBranch (false);
        SetUnShadow ();
        SetShadowForCardsHaveTypeBranch ();
    }
    public void SetUpdateListHoldNotShadow(){
        for (int i=0;i<3;i++)
        {
            SetUnChildToSort(i);
            for (CardModel card:listChi.get(i))
                card.SetParent(index,i);
            SortList(i);
        }
    }

    public void OpenList(int index){
        for (CardModel cardModel:listChi.get(index))
            cardModel.SetVisible(true);
    }
    public void OpenAllList(){
        for(int i=0;i<3;i++)
            OpenList(i);
    }
    public void SwapTwoCards(int index_1,int index_2){
        Array<CardModel> arr = GetListCards();
        arr.swap(index_1,index_2);
        ClearChi();
        for (int i=0;i<arr.size;i++)
            AddNewCard(arr.get(i));
        SetUpdateListHold();
    }
    void ClearChi(){
        for (Array<CardModel> arr:listChi)
            arr.clear();
    }
    public void SortBranch(){
        for (int i=0;i<listChi.size;i++)
        {
            GameModel.Instance.SortBranch (listChi.get(i));
        }
        SetSort ();
    }
    public void SetCompletedSort(){
        if (index == 0)
            SetLockTypeBranch ();
        SetLockSort (true);
        isCompletedSort = true;
        ShowSorting (false);
        GameModel.Instance.CheckAllPlayerCompletedSort ();
    }

    public interface OnShowSorting{
        public void OnEvent(boolean is);
    }
    OnShowSorting cbShowSorting;
    public void ShowSorting(boolean isShow)
    {
        if (cbShowSorting != null)
            cbShowSorting.OnEvent (isShow);
    }
    public void RegisterShowSorting(OnShowSorting cb)
    {
        cbShowSorting = cb;
    }

    public Array<CardModel> GetListContainCard(CardModel card)
    {
        for (Array<CardModel> list : listChi)
        if (list.contains(card,false))
            return list;
        return null;
    }
    public void SetSort(){
        SetUpdateListHold();
    }
    public void AddResult(PlayerModel player,int result,int score)
    {
        listResultWithPlayer.put(player,listResultWithPlayer.get(player)+result);
        listScoreWithPlayer.put(player,listScoreWithPlayer.get(player)+result * score);
        listScoreWithPlayerOfBranch.put(player,result * score);
        listResult.add (0);
    }
    public void SetWinAbsoluteWithAllPlayers(){
        for (int i = 0; i < 3; i++)
            SetUnShadowForBranch (i);
        int score_tmp = 0;
        for (PlayerModel player : GameModel.Instance.listPlayer) {
            if (player == this)
                continue;
            score_tmp = GameModel.Instance.GetScoreWhenWinAbsolute (typeWinAbsolute);
            player.scoreChange -= score_tmp;
        }

        this.scoreChange += score_tmp*(GameModel.Instance.listPlayer.size-1);
        SetReportWinAbsolute (typeWinAbsolute);
        SetOpenAllBranch (true);
    }

    public interface OnReportWinAbsolute{
        public void OnEvent(GameModel.TypeWinAbsolute type);
    }
    OnReportWinAbsolute cbReportWinAbsolute;
    void SetReportWinAbsolute(GameModel.TypeWinAbsolute type)
    {
        if (cbReportWinAbsolute != null)
            cbReportWinAbsolute.OnEvent (type);
    }
    public void RegisterReportWinAbsolute(OnReportWinAbsolute cb)
    {
        cbReportWinAbsolute =cb;
    }

    public interface OnLockTypeBranch{
        public void OnEvent();
    }
    OnLockTypeBranch cbLockTypeBranch;
    public void SetLockTypeBranch()
    {
        if (cbLockTypeBranch != null)
            cbLockTypeBranch.OnEvent ();
    }
    public void RegisterSetLockTypeBranch(OnLockTypeBranch cb)
    {
        cbLockTypeBranch =cb;
    }

    public interface OnShowTypeBranch{
        public void OnEvent(int i);
    }
    OnShowTypeBranch cbShowTypeBranch;
    public void SetShowTypeBranch(int index)
    {
        if (cbShowTypeBranch != null)
            cbShowTypeBranch.OnEvent (index);
    }
    public void RegisterShowTypeBranch(OnShowTypeBranch cb)
    {
        cbShowTypeBranch =cb;
    }

    public void SetShadowForBranch(int index_branch,int level)
    {
        for (CardModel card : listChi.get(index_branch))
        card.SetShadow (level);
    }

    public void SetOpenAllBranch(boolean isOpen){
        for (int i = 0; i < 3; i++)
            SetOpenBranch (i, isOpen);
    }
    MyLinkedMap<PlayerModel,Integer> listResultWithPlayer;
    MyLinkedMap<PlayerModel,Integer> listScoreWithPlayer;
    MyLinkedMap<PlayerModel,Integer> listScoreWithPlayerOfBranch;
    public void ResetListResult(){
        isLoseThreeBranch = false;
        listResult.clear();
        scoreChange = 0;
        CreateListResult ();

        GameModel.Instance.listPlayerExists.clear ();
        GameModel.Instance.listPlayerExists.addAll (GameModel.Instance.listPlayer);
        //SetUnShadow ();
    }

    public void CreateListResult()
    {
        listResultWithPlayer.clear();
        listScoreWithPlayer.clear();
        listScoreWithPlayerOfBranch.clear();

        for (PlayerModel player : GameModel.Instance.listPlayer) {
        if (player == this)
            continue;
        listResultWithPlayer.put (player,0);
        listScoreWithPlayer.put (player,0);
        listScoreWithPlayerOfBranch.put (player,0);
        }
    }
    public void ChangeScoreNotHaveInfo(int score){
        Preferences preferences=PreferenceController.Instance.preferences;
        long coin = Long.parseLong(preferences.getString("HighScore"));
        coin+=score;
        preferences.putString("HighScore",coin+"");
        preferences.flush();
        MauBinh.zenSDK.FBInstant_IncrementDoubleStats("money", score, new IZen.FBInstant_IncrementDoubleStatsCallback() {
            @Override
            public void OnSuccess(double newvalue) {
                if (newvalue<50000) {
                    newvalue = 50000;
                    MauBinh.zenSDK.FBInstant_SetDoubleStats("money",newvalue, null);
                }

                MauBinh.zenSDK.ReportScore("", (long) newvalue);
                PreferenceController.Instance.preferences.putString("HighScore", (long)newvalue+"");
                PreferenceController.Instance.preferences.flush();
            }
        });
    }

    public void SetScoreSummary(){
        if (cbChangeScore != null)
            cbChangeScore.OnEvent (scoreChange);

        if (!GameModel.Instance.is_end)
        {
            infoModel.coin+=scoreChange;
            Preferences preferences=PreferenceController.Instance.preferences;
            if (index==0){
                if (index==0 && infoModel.coin<50000)
                    infoModel.coin=50000;
                preferences.putString("HighScore",String.valueOf(infoModel.coin));
                MauBinh.zenSDK.FBInstant_IncrementDoubleStats("money", scoreChange, new IZen.FBInstant_IncrementDoubleStatsCallback() {
                    @Override
                    public void OnSuccess(double newvalue) {
                        infoModel.coin = (long)newvalue;
                        if (infoModel.coin<50000) {
                            infoModel.coin = 50000;
                            MauBinh.zenSDK.FBInstant_SetDoubleStats("money", infoModel.coin, null);
                        }

                        MauBinh.zenSDK.ReportScore("", infoModel.coin);
                        PreferenceController.Instance.preferences.putString("HighScore", String.valueOf(infoModel.coin));
                    }
                });
            }
            else
                preferences.putString("ScoreOfPlayer"+infoModel.index,String.valueOf(infoModel.coin));
            preferences.flush();

            SetInfo();
        }
    }
    public void SetUnShadowForBranch(int index_branch)
    {
        for (int i=0;i<listChi.size;i++)
        {
            Array<CardModel> chi = listChi.get(i);
            for (int j=0;j<chi.size;j++)
            {
                CardModel card = chi.get(j);
                card.SetShadow(0);
            }
        }
    }
    public void SetOpenBranch(int index,boolean isOpen)
    {
        for (CardModel card : listChi.get(index))
        card.SetVisible (isOpen);
    }
    public void SetFlipBranch(int index){
        Array<CardModel> chi = listChi.get(index);
        for (int i=0;i<chi.size;i++)
            chi.get(i).SetFlip(i);
    }
    public void SetPriorityBranch(int index,boolean is_pri)
    {
        for (int i = 0; i < listChi.get(index).size; i++)
            listChi.get(index).get(i).SetPriority (is_pri);
    }

    Array<Integer> listResult;
    public boolean IsCompleteCount(int index_branch)
    {
        return listResult.size >= (index_branch+1) * (GameModel.Instance.GetListPlayerExist().size-1);
    }

    public interface OnReportSap3Chi{
        public void OnEvent();
    }
    OnReportSap3Chi cbReportSap3Chi;
    public void SetReportSap3Chi()
    {
        if (cbReportSap3Chi!=null)
            cbReportSap3Chi.OnEvent();
    }
    public void RegisterReportSap3Chi(OnReportSap3Chi cb)
    {
        cbReportSap3Chi =cb;
    }
    public void SetWinAll(){
        SetUnShadow ();
        int sum = 0;
        for (int i=0;i<listResultWithPlayer.size();i++)
        {
            int score = listScoreWithPlayer.getValue(i)*GameModel.Instance.listPlayer.size/2;
            listResultWithPlayer.getEntry(i).getKey().SetChangeScore (-score);
            sum += score;
        }
        SetChangeScore (sum);
        SoundController.Instance.PlayClipCoin();
    }
    public void SetUnShadowCardsAce(boolean is_pri)
    {
        for (int i=2;i>=0;i--) {
            Array<CardModel> list = listChi.get(i);
            for (CardModel card : list)
        if (card.number == 1) {
            card.SetShadow (0);
            card.SetPriority (true);
        }
    }
    }
    GameModel.TypeWinAbsolute typeTmptWinAbsolute;
    public void SetWinAbsolute(GameModel.TypeWinAbsolute type){
        if (index == 0) {
            if (type == GameModel.TypeWinAbsolute.RONGLOC || type == GameModel.TypeWinAbsolute.SANHRONG || type == GameModel.TypeWinAbsolute.CUNGMAU13 || type == GameModel.TypeWinAbsolute.CUNGMAU12) {
                typeWinAbsolute = type;
                SetLockSort (true);
            } else {
                typeTmptWinAbsolute = type;
            }
            SetHaveWinAbsolute (type);
        }
        else
            typeWinAbsolute = type;
    }

    public interface OnHaveWinAbsolute{
        public void OnEvent(GameModel.TypeWinAbsolute type);
    }
    OnHaveWinAbsolute cbHaveWinAbsolute;
    void SetHaveWinAbsolute(GameModel.TypeWinAbsolute type)
    {
        if (cbHaveWinAbsolute != null)
            cbHaveWinAbsolute .OnEvent(type);
    }
    public void RegisterHaveWinAbsolute(OnHaveWinAbsolute cb)

    {
        cbHaveWinAbsolute =cb;
    }

    public void SetLockSort(boolean isLock)
    {
        for (Array<CardModel> list : listChi) {
            for (CardModel card : list)
                card.SetCanClick (!isLock);
    }
    }

    public void SetPlayerLoseBranch(){
        Array<PlayerModel> listPlayer = GetListPlayerLoseThreeBranch();
        for (PlayerModel player : listPlayer) {
            int score = GetScoreChangeWithPlayer (player);
            ChangeScoreToCount (score);
            player.ChangeScoreToCount (-score);
            isLoseThreeBranch = true;
        }
    }
    int GetScoreChangeWithPlayer(PlayerModel player)
    {
        return listScoreWithPlayer.get(player);
    }
    public void ChangeScoreToCount(int value_change)
    {
        this.scoreToCount += value_change;
    }
    public Array<PlayerModel> GetListPlayerLoseThreeBranch()
    {
        Array<PlayerModel> listPlayer = new Array<PlayerModel> ();
        for (int i=0;i<listResultWithPlayer.size();i++)
        {
            Map.Entry entry = listResultWithPlayer.getEntry(i);
            if ((Integer)entry.getValue()==-3)
                listPlayer.addAll((PlayerModel) entry.getKey());
        }
        return listPlayer;
    }

    public interface OnWinOrLose{
        public void OnEvent(int score);
    }
    OnWinOrLose cbWinOrLose;
    public void SetWinOrLose()
    {
        if (cbWinOrLose != null)
            cbWinOrLose.OnEvent (scoreChange);
    }
    public void RegisterSetWinOrLose(OnWinOrLose cb)
    {
        cbWinOrLose =cb;
    }

    public boolean IsWinAll(){
        for (int i=0;i<listResultWithPlayer.size();i++) {
            if (listResultWithPlayer.getValue(i) < 3)
                return false;
        }
        return true;
    }
    public void SetBinhLung(Array<PlayerModel> listValid)
    {
        for (int i = 0; i < 3; i++)
            SetUnShadowForBranch (i);
        int scoreWin = 0;
        for (PlayerModel player : listValid) {
        for (int i = 0; i < 3; i++) {
            scoreWin = GameModel.Instance.GetScoreWinOfBranch (player.listChi.get(i), i);
            scoreWin *= 2;
            this.scoreToCount -= scoreWin;
            player.scoreToCount += scoreWin;
        }
    }
        SetReportLungBai ();
        SetOpenAllBranch (true);
    }

    public interface OnReportLungBai{
        public void OnEvent();
    }
    OnReportLungBai cbReportLungBai;
    void SetReportLungBai()
    {
        if (cbReportLungBai != null)
            cbReportLungBai.OnEvent();
    }
    public void RegisterReportLungBai(OnReportLungBai cb)
    {
        cbReportLungBai =cb;
    }

    public void OnRemove(){
        for(Array<CardModel> list : listChi) {
            for (CardModel card : list)
             card.SetShadow (0);
        }
    }
    public void SetCountAce(){int dem = 0;
        for(Array<CardModel> list : listChi)
        {
            for (CardModel card : list)
            if (card.number == 1)
                dem++;
        }
        scoreToCount += (GameModel.Instance.listPlayer.size*dem-GameModel.Instance.GetSumAce ()) * GameModel.Instance.scoreBet;
    }
    public void SetUnShadow(){
        for (Array<CardModel> list : listChi) {
            for (CardModel card : list)
            card.SetShadow (0);
        }
    }
    void SetShadow()
    {
        for (Array<CardModel> list : listChi) {
        for (CardModel card : list)
        card.SetShadow (1);
    }
    }
    void SetShadowForCardsHaveTypeBranch()
    {
        if (index != 0)
            return;
        Array<CardModel> list_rac = new Array<CardModel> ();
        Array<Array<CardModel>> list_tmp = new Array<Array<CardModel>> ();
        while (list_tmp.size < 3)
            list_tmp.add (new Array<CardModel>());
        for (int i = 0; i < 3; i++)
            list_tmp .get(i).addAll (listChi.get(i));
        for (Array<CardModel> list : list_tmp) {
        list_rac.addAll (GameModel.Instance.GetListRac(list));
        }
        for (Array<CardModel> list : listChi) {
            for (CardModel card : list)
        if (!list_rac.contains (card,false))
            card.SetShadow (1);
       }
    }
    public void SetTypeBranch(boolean isSpecial)
    {
        for (int i = 0; i < listChi.size; i++) {
            GameModel.TypeBranch typeBranch = GameModel.Instance.GetTypeBranch (listChi.get(i));
            SetTypeBranch (i,typeBranch,isSpecial);
        }

        SetValidBranch ();

        UIController.Instance.sortCardsUI.btnMauBinh.setVisible(false);

        GameModel.Instance.CheckWinAbsolute (this);
    }
    public interface OnSetTypeBranch{
        public void OnEvent(int a, GameModel.TypeBranch b, boolean c, int number);
    }
    Array<OnSetTypeBranch> cbSetTypeBranch=new Array<OnSetTypeBranch>();

    void SetTypeBranch(int index_branch, GameModel.TypeBranch type_branch,boolean isSpecial)
    {
        for (OnSetTypeBranch cb:cbSetTypeBranch)
            cb.OnEvent (index_branch,type_branch,isSpecial,listChi.get(index_branch).get(0).number);
    }
    public void RegisterSetTypeBranch(OnSetTypeBranch cb)
    {
        cbSetTypeBranch.add(cb);
    }


    public GameModel.TypeWinAbsolute typeWinAbsolute;
    public boolean isLoseThreeBranch;
    public int scoreToCount;
    public int scoreChange;
    public boolean isHost;

    //cb
    OnSetUnChildToSort cbSetUnChildToSort;

    void SetUnChildToSort(int index_list){
        if (cbSetUnChildToSort!=null)
            cbSetUnChildToSort.OnEvent(index_list);
    }
    public void RegisterSetUnChildToSort(OnSetUnChildToSort onSetUnChildToSort){
        this.cbSetUnChildToSort=onSetUnChildToSort;
    }
    public interface OnSetUnChildToSort{
        public void OnEvent(int index_list);
    }


    OnSetInfo cbSetInfo;
    public void SetInfo(){
        if (cbSetInfo!=null)
            cbSetInfo.OnEvent();
    }
    public void RegisterSetInfo(OnSetInfo cb)
    {
        cbSetInfo=cb;
    }
    public interface OnSetInfo{
        public void OnEvent();
    }

    public void Clear(){
        for (Array<CardModel> arr:listChi)
            arr.clear();
        if (infoModel!=null)
           infoModel.Clear();
        isCompletedSort=false;
        typeTmptWinAbsolute= GameModel.TypeWinAbsolute.KHONGANTRANG;
        typeWinAbsolute= GameModel.TypeWinAbsolute.KHONGANTRANG;
        scoreChange=0;
    }
    public void ChangeScore(int index_branch)
    {
        int score = 0;
        for(int i=0;i<listScoreWithPlayerOfBranch.size();i++)
        {
            score += listScoreWithPlayerOfBranch.getValue(i);
        }
        SetChangeScore (score);
    }
    public void SetChangeScoreSummaryView(){
        if (cbChangeScore!=null)
            cbChangeScore.OnEvent(scoreChange);
    }
    public void SetChangeScore(int score)
    {
        scoreChange += score;
//        Gdx.app.log("Player "+index,score+"");
        if (cbChangeScore!=null)
            cbChangeScore.OnEvent(score);
    }
    public Array<CardModel> GetListCards(){
        Array<CardModel> arr = new Array<CardModel>();
        for (Array<CardModel> chi:listChi)
            arr.addAll(chi);
        return arr;
    }

    public interface OnChangeScore{
        public void OnEvent(int score);
    }
    OnChangeScore cbChangeScore;
    public void RegisterChangeScore(OnChangeScore cb)
    {
        cbChangeScore=cb;
    }

    Array<OnSortList> onSortList=new Array<OnSortList>();
    public void SortList(int index_list){
        if (onSortList!=null)
        {
            for(OnSortList cb:onSortList)
                cb.OnEvent(index_list);
        }
    }
    public void RegisterSortList(OnSortList onSortList){
        this.onSortList.add(onSortList);
    }
    public interface OnSortList{
        public void OnEvent(int index_list);
    }

    public interface OncbBranchIsValid{
        public void OnEvent(int i,boolean b);
    }
    OncbBranchIsValid cbBranchIsValid;
    void SetBranchIsValid(int index, boolean is_valid)
    {
        if (cbBranchIsValid != null)
            cbBranchIsValid.OnEvent (index,is_valid);
    }
    public void RegisterBranchIsValid(OncbBranchIsValid cb)
    {
        cbBranchIsValid =cb;
    }

    void SetValidBranch()
    {
        Array<Integer> list_invalid = ListBranchLung ();
        for (int i=0;i<3;i++)
        {
            if (list_invalid.get(i) == -1)
                SetBranchIsValid (i, false);
            else
                SetBranchIsValid (i,true);
        }
    }
    Array<Integer> ListBranchLung()
    {
        Array<Integer> list = new Array<Integer> ();
        while (list.size < 3)
            list.add (1);
        if (GameModel.Instance.CompareTwoBranch (listChi.get(0),listChi.get(1))<1) {
            list .set(0,-1);
            list .set(1,-1);
        }
        if (GameModel.Instance.CompareTwoBranch (listChi .get(1),listChi .get(2))<1) {
            list .set(1,-1);
            list .set(2,-1);
        }
        return list;
    }
    public void SetOnMauBinh(){
        typeWinAbsolute = typeTmptWinAbsolute;
        SetLockSort(true);
    }
}
