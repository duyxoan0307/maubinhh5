package com.mygdx.catte.Model;

import com.badlogic.gdx.utils.Array;

import java.util.Set;

/**
 * Created by DUY on 6/12/2018.
 */

public class CardModel {
    public enum CardType{BICH,CHUON,RO,CO};
    public int number;
    public CardType type;

    public CardModel(int number, CardType cardType){
        this.number=number;
        this.type=cardType;
    }
    public int GetNumberType2()
    {
        if (this.number == 1)
            return 14;
        return this.number;
    }

    public static int CompareCards(CardModel card_model_1,CardModel card_model_2)
    {
        if (card_model_1.number == card_model_2.number) {
            if (card_model_1.type.ordinal() < card_model_2.type.ordinal())
                return -1;
            else
            if (card_model_1.type.ordinal() > card_model_2.type.ordinal())
                return 1;
            else
                return 0;
        }
        if (card_model_1.number > card_model_2.number){
            return 1;
        }
        return -1;
    }


    public static int CompareCardsType2(CardModel card_model_1,CardModel card_model_2)
    {
        if (card_model_1.GetNumberType2() == card_model_2.GetNumberType2()) {
            if (card_model_1.type.ordinal() < card_model_2.type.ordinal())
                return -1;
            else
            if (card_model_1.type.ordinal() > card_model_2.type.ordinal())
                return 1;
            else
                return 0;
        }
        if (card_model_1.GetNumberType2() > card_model_2.GetNumberType2()){
            return 1;
        }
        return -1;
    }

    public static int CompareNumber(CardModel card_model_1,CardModel card_model_2)
    {
        if (card_model_1.number == card_model_2.number) {
            return 0;
        }
        if (card_model_1.number == 1)
            return 1;
        if (card_model_2.number == 1)
            return -1;
        if (card_model_1.number > card_model_2.number){
            return 1;
        }
        return -1;
    }

    public static boolean IsSameType(CardModel card_model_1,CardModel card_model_2)
    {
        return (card_model_1.type == card_model_2.type);
    }
    public static boolean IsSameNumber(CardModel card_model_1,CardModel card_model_2)
    {
        return (card_model_1.number == card_model_2.number);
    }

    public static String GetNameTypeCard(CardType type)
    {
        switch (type) {
            case BICH:
                return "BÍCH";
            case CHUON:
                return "CHUỒN";
            case RO:
                return "RÔ";
            case CO:
                return "CƠ";
        }
        return "";
    }
    public static String GetNameTypeCardEnglish(CardType type)
    {
        switch (type) {
            case BICH:
                return "SPADE";
            case CHUON:
                return "CLUB";
            case RO:
                return "DIAMOND";
            case CO:
                return "HEART";
        }
        return "";
    }
    public String toString(){
        return this.number+" "+GetNameTypeCard(this.type);
    }


    OnSetParent cbSetParent;
    public void SetParent(int index_player,int index_list){
        if(cbSetParent!=null)
            cbSetParent.OnEvent(index_player,index_list);
    }
    public void RegisterOnSetParent(OnSetParent cbOnSetParent){
        this.cbSetParent = cbOnSetParent;
    }
    public interface OnSetParent{
        public void OnEvent(int index_player,int index_list);
    }

    OnSetVisible cbOnSetVisible;
    public void SetVisible(boolean is_visible){
        if (cbOnSetVisible!=null)
            cbOnSetVisible.OnEvent(is_visible);
    }
    public void RegisterSetVisible(OnSetVisible cb){
        this.cbOnSetVisible=cb;
    }
    public void RemoveRegisterSetVisible(){
        this.cbOnSetVisible=null;
    }
    public interface OnSetVisible{
        public void OnEvent(boolean is_visible);
    }

    OnSetFlip cbSetFlip;
    public void SetFlip(int index){
        if (cbSetFlip!=null)
            cbSetFlip.OnEvent(index);
    }
    public void RegisterSetFlip(OnSetFlip cb){
        this.cbSetFlip=cb;
    }
    public void RemoveRegisterSetFlip(){
        this.cbSetFlip=null;
    }
    public interface OnSetFlip{
        public void OnEvent(int index);
    }

    OnSetValid cbSetValid;
    public void SetValid(){
        if (cbSetValid!=null)
            cbSetValid.OnEvent();
    }
    public void RegisterSetValid(OnSetValid cb){
        cbSetValid=cb;
    }
    public void RemoveRegisterSetValid(){
        cbSetValid=null;
    }
    public interface OnSetValid{
        public void OnEvent();
    }

    OnSetCanClick cbSetCanClick;
    public void SetCanClick(boolean can_click){
        if (cbSetCanClick!=null)
            cbSetCanClick.OnEnvent(can_click);
    }
    public void RegisterSetCanClick(OnSetCanClick cb){
        cbSetCanClick=cb;
    }
    public void RemoveRegisterSetCanClick(){
        cbSetCanClick=null;
    }
    public interface OnSetCanClick{
        public void OnEnvent(boolean can_click);
    }

    OnSetBaiCup cbSetBaiCup;
    public void SetBaiCup(){
        if (cbSetBaiCup!=null)
            cbSetBaiCup.OnEvent();
    }
    public void RegisterSetBaiCup(OnSetBaiCup cb)
    {
        cbSetBaiCup=cb;
    }
    public void RemoveRegisterSetBaiCup()
    {
        cbSetBaiCup=null;
    }
    public interface OnSetBaiCup{
        public void OnEvent();
    }

    Array<OnSetShadow> cbSetShadow=new Array<OnSetShadow>();
    public void SetShadow(int level){
        for (OnSetShadow cb:cbSetShadow)
            cb.OnEvent(level);
    }
    public void RegisterSetShadow(OnSetShadow cb){
        cbSetShadow.add(cb);
    }
    public void UnRegisterSetShadow(OnSetShadow cb){
        cbSetShadow.removeValue(cb,false);
    }
    public void RemoveRegisterSetShadow(){
        cbSetShadow=null;
    }
    public interface OnSetShadow{
        public void OnEvent(int level);
    }

    OnSetNormal cbSetNormal;
    public void SetNormal(){
        if (cbSetNormal!=null)
            cbSetNormal.OnEvent();
    }
    public void RegisterSetNormal(OnSetNormal cb){
        cbSetNormal=cb;
    }
    public void RemoveRegisterSetNormal(){
        cbSetNormal=null;
    }
    public interface OnSetNormal{
        public void OnEvent();
    }

    OnSetFirstDistribute cbSetFirstDistribute;
    public void SetFirstDistribute(){
        if (cbSetFirstDistribute!=null)
            cbSetFirstDistribute.OnEvent();
    }
    public void RegisterSetFirstDistribute(OnSetFirstDistribute cb){
        cbSetFirstDistribute=cb;
    }

    public interface OnSetFirstDistribute{
        public void OnEvent();
    }

    public interface OnCanBocHoacAn{
        public void OnEvent(int type);
    }
    OnCanBocHoacAn cbCanBocHoacAn;
    public void SetCanBocHoacAn(int type)
    {
        if (cbCanBocHoacAn!=null)
            cbCanBocHoacAn.OnEvent(type);
    }
    public void RegisterCanBocHoacAn(OnCanBocHoacAn cb)
    {
        cbCanBocHoacAn=cb;
    }

    public interface OnPriority{
        public void OnEvent(boolean b);
    }
    OnPriority cbPriority;
    public void SetPriority(boolean is_priority)
    {
        if (cbPriority != null)
            cbPriority.OnEvent (is_priority);
    }
    public void RegisterSetPriority(OnPriority cb)
    {
        cbPriority = cb;
    }
    OnRemoveActor cbRemoveActor;
    public void SetRemoveActor(boolean is_immediately){
        if (cbRemoveActor!=null)
            cbRemoveActor.OnEvent(is_immediately);
    }
    public void RegisterRemoveActor(OnRemoveActor cb){
        cbRemoveActor=cb;
    }
    public void RemoveRegisterRemoveActor(){
        cbRemoveActor=null;
    }
    public interface OnRemoveActor{
        public void OnEvent(boolean is_immediately);
    }
}
