package com.mygdx.catte.Model;

import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.CharArray;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.Utilities;
import com.mygdx.catte.View.CardView;

import javax.rmi.CORBA.Util;

/**
 * Created by DUY on 7/18/2018.
 */

public class DataModel {
    public static DataModel Instance;
    Preferences preferences;
    GameModel gameModel;
    public DataModel(){
        Instance=this;
        preferences=PreferenceController.Instance.preferences;
        gameModel=GameModel.Instance;
    }
    public void SavePlayState(){
        preferences.putInteger("PlayState",gameModel.playState.ordinal());
        preferences.flush();
    }
    public void SaveCards(){
        for (int i=0;i<gameModel.listPlayer.size;i++)
        {
            PlayerModel player = gameModel.listPlayer.get(i);
            for (int j=0;j<3;j++)
            {
                Utilities.SaveListCardToPreference(player.listChi.get(j),"Chi"+j+"Player"+player.index);
            }
        }
        preferences.flush();
    }
    public void SaveListHost(){
        Utilities.SaveListCardToPreference(gameModel.listHost,"ListHost");
    }
    public void SaveGameState(){
        preferences.putInteger("GameState",gameModel.gameState.ordinal());
        preferences.flush();
    }

    public void Reset(){
        ResetData();
    }
    public static void SReset(){
        Preferences preferences=PreferenceController.Instance.preferences;
        if (preferences.contains("GameState"))
            preferences.remove("GameState");
        for (int i=0;i<4;i++)
        {
            for (int j=0;j<3;j++)
            {
                String pref = "Chi"+j+"Player"+i;
                if (preferences.contains(pref))
                    preferences.remove(pref);
            }
            if (preferences.contains("AnTrang"+i))
                preferences.remove("AnTrang"+i);
        }
        preferences.flush();
    }
    void ResetData(){
        if (preferences.contains("GameState"))
            preferences.remove("GameState");
        for (int i=0;i<4;i++)
        {
            for (int j=0;j<3;j++)
            {
                String pref = "Chi"+j+"Player"+i;
                if (preferences.contains(pref))
                    preferences.remove(pref);
            }
            if (preferences.contains("AnTrang"+i))
                preferences.remove("AnTrang"+i);
        }
        preferences.flush();
    }
    public void SaveAnTrang(){
        for (int i=0;i<gameModel.listPlayer.size;i++)
        {
            PlayerModel player = gameModel.listPlayer.get(i);
            preferences.putInteger("AnTrang"+player.index,player.typeWinAbsolute.ordinal());
        }
        preferences.flush();
    }
    public void SaveListCardOnTable()
    {
        Utilities.SaveListCardToPreference(GameModel.Instance.listCardOnTable,"ListCardOnTable");
    }
    public boolean IsResume(){
        if (!PreferenceController.Instance.preferences.contains("GameState"))
            return false;
        return PreferenceController.Instance.preferences.getInteger("GameState")!= GameModel.GameState.OVER.ordinal();
    }
    public void LoadData(){
        gameModel.playState=GameModel.PlayState.values()[preferences.getInteger("PlayState")];
        gameModel.gameState=GameModel.GameState.values()[preferences.getInteger("GameState")];
        gameModel.isPlaying=true;
        for (int i=0;i<gameModel.listPlayer.size;i++)
        {
            PlayerModel player = gameModel.listPlayer.get(i);
            for (int j=0;j<3;j++)
            {
                Utilities.ImportDataCards(preferences.getString("Chi"+j+"Player"+i),gameModel.listPlayer.get(i).listChi.get(j));
                gameModel.listCardOnTable.removeAll(player.listChi.get(j),false);
            }
            gameModel.listPlayer.get(i).typeWinAbsolute= GameModel.TypeWinAbsolute.values()[preferences.getInteger("AnTrang"+i)];
        }
        Utilities.ImportDataCards(preferences.getString("ListHost"),gameModel.listHost);
        for (int i=0;i<gameModel.listPlayer.size;i++)
        {
            PlayerModel player = gameModel.listPlayer.get(i);
            player.SetUpdateListHold();
        }
    }
}
