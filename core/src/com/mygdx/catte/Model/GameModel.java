package com.mygdx.catte.Model;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.Controller.BotController.BotBasicController;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.MyLinkedMap;

import java.util.Comparator;
import java.util.Map;


/**
 * Created by DUY on 6/12/2018.
 */

public class GameModel {
    public static GameModel Instance;
    public  BoardModel boardModel;
    public Array<PlayerModel> listPlayerInit;
    public  Array<PlayerModel> listPlayer;
    Array<PlayerModel> listPlayerExists;
    public  Array<CardModel> listCardOnTable;
    public int currentTurn;
    public boolean isPlaying=false;
    boolean isDestroy;
    public boolean is_end=false;
    public enum GameState
    {
        START,
        PLAYING,
        PAUSED,
        OVER
    };
    public GameState gameState;
    public int scoreBet;
    public int host;

    public enum TypeBranch{THUNGPHASANH,TUQUY,CULU,THUNG,SANH,XAMCO,THU,DOI,MAUTHAU};
    boolean isAce;

    public enum PlayState{CHUANHANBAI,MAYXEPBAI,PLAYERXEPBAI,TINHTIEN,TINHTIENXONG};
    public PlayState playState;

    int gameCount =0;

    public GameModel(int score_bet,int number_player,boolean is_resume){
        Instance = this;
        boardModel = new BoardModel();
        listPlayerInit = new Array<PlayerModel>();
        listPlayer = new Array<PlayerModel>();

        for (int i = 0; i < 4; i++) {
            PlayerModel player_model = new PlayerModel(this);
            BotBasicController bot;
            listPlayerInit.add(player_model);
            player_model.index=i;
        }
        listCardOnTable = new Array<CardModel>();
        listCardOnTable.addAll(boardModel.listCard);
        listCardOnTable.shuffle();

        listHost = new Array<CardModel> ();

        scoreBet = 2;
        gameState = GameState.OVER;
        host = PreferenceController.Instance.preferences.getInteger("Host");
        isDestroy=false;
        isAce=true;


        listPlayerExists = new Array<PlayerModel> ();
        listPlayerExists.addAll (listPlayer);
        //test
        // TestCompareList();
//        SapXep();
    }
    void SetListHost()
    {
        int current_host=PreferenceController.Instance.preferences.getInteger("CurrentHost");
        for (Array<CardModel> list : listPlayer.get(current_host).listChi) {
            for (CardModel card : list)
                listHost.add (card);
        }
    }
    public void DistributeCardsTest(){
        //       host=1;
        isPlaying=true;
        int count=listCardOnTable.size;
        int count_loop=-1;

        final Array<CardModel> arrCardTest= new Array<CardModel>();

//        arrCardTest.clear();
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(1, CardModel.CardType.BICH))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(7, CardModel.CardType.CHUON))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(10, CardModel.CardType.CHUON))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(6, CardModel.CardType.BICH))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(6, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(2, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(4, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(5, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(11, CardModel.CardType.BICH))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(11, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(9, CardModel.CardType.CO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(9, CardModel.CardType.RO))));
//        arrCardTest.add(BoardModel.Instance.listCard.get(BoardModel.Instance.GetIndexOfCardOtherBoard(new CardModel(9, CardModel.CardType.BICH))));


        listCardOnTable.removeAll(arrCardTest,false);
        float time_delay=0.01f;
        while (count>(52 - (listPlayer.size*13))) {
            for (int i = 0; i < listPlayer.size; i++)
            {
                if (count<=(52 - (listPlayer.size*13)))
                    break;
                count_loop++;
                final int finalI = i;
                final int finalCount_loop = count_loop;
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        if (isPlaying)
                        {
                            CardModel card_model_random;
                            PlayerModel player = listPlayer.get((host+ finalI)%(listPlayer.size));
                            if (player.index==0 && arrCardTest.size>player.GetListCards().size)
                            {
                                card_model_random=arrCardTest.get(player.GetListCards().size);
                            }
                            else
                            {
                                card_model_random = GetRandomCardFromBoard();
                            }
                            player.AddNewCard (card_model_random);
                            card_model_random.SetFirstDistribute();
                            if (finalCount_loop %6==0)
                                SoundController.Instance.PlayClipPlayCard();
                        }
                    }
                },time_delay*(count_loop));
                count--;
            }
        }
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (isPlaying) {
                    SetListHost();
                    SetEndDistributeCards();
                }
            }
        },time_delay*(count_loop)+1);

    }
    CardModel GetRandomCardFromBoard()
    {
        if (listCardOnTable.size == 0)
            return null;
        CardModel card_random = listCardOnTable.peek();
        listCardOnTable.removeValue(card_random,false);
        UIController.Instance.playingUI.SetTextSoBai();
        return card_random;
    }
    int ConvertTurn(int turn)
    {
        if (turn >= 4)
            return turn % 4;
        if (turn < 0)
            return turn + 4;
        return turn;
    }
    public PlayerModel GetMainPlayer()
    {
        return listPlayerInit.get(0);
    }
    public static void SwapTwoCardsInList(Array<CardModel> list_card_model,CardModel card_model_1, CardModel card_model_2){
        CardModel card_model_temp = card_model_1;
        int index_card_1 = list_card_model.indexOf(card_model_1,false);
        int index_card_2 = list_card_model.indexOf(card_model_2,false);
        list_card_model.set(index_card_1,card_model_2);
        list_card_model.set(index_card_2,card_model_temp);
    }

    public static void SortCardInList(Array<CardModel> list_card_model, final boolean isDESC){
        if (list_card_model == null)
            return;
        list_card_model.sort(new Comparator<CardModel>() {
            @Override
            public int compare(CardModel cardModel, CardModel t1) {
                return isDESC?CardModel.CompareCards(t1,cardModel):CardModel.CompareCards(cardModel,t1);
            }
        });
    }
    void SortCardInListType2(Array<CardModel> list_card_model, final boolean isDESC)
    {

        if (list_card_model == null)
            return;
        list_card_model.sort(new Comparator<CardModel>() {
            @Override
            public int compare(CardModel cardModel, CardModel t1) {
                return isDESC?CardModel.CompareCardsType2(t1,cardModel):CardModel.CompareCardsType2(cardModel,t1);
            }
        });
    }
    void DeleteCardByList(Array<CardModel> list_to_delete, Array<CardModel> list_card)
    {
        list_to_delete.removeAll(list_card,false);
    }

    OnEndDistributeCards cbEndDistributeCards;
    void SetEndDistributeCards(){
        if (cbEndDistributeCards!=null)
            cbEndDistributeCards.OnEnvent();
    }
    public void RegisterEndDistributeCards(OnEndDistributeCards cb){
        this.cbEndDistributeCards=cb;
    }
    public interface OnEndDistributeCards{
        public void OnEnvent();
    }
    public void Clear(){
        for (PlayerModel player:listPlayerInit)
            player.Clear();
        listPlayer.clear();
        for (int i=0;i<PreferenceController.Instance.preferences.getInteger("NumberPlayer");i++)
            listPlayer.add(listPlayerInit.get(i));
        listCardOnTable.clear();
        listCardOnTable.addAll(boardModel.listCard);
        listCardOnTable.shuffle();
        listHost.clear();

        scoreBet = GameController.GetScoreBet();
        gameState = GameState.OVER;
        host = PreferenceController.Instance.preferences.getInteger("Host");
        currentTurn=host;
        isDestroy = false;
        isCounting=false;
        isAce= GameSetting.Instance.isAce;
        for (CardModel card:BoardModel.Instance.listCard)
            card.cbSetShadow.clear();
        is_end=false;
        playState=PlayState.CHUANHANBAI;
    }
    public void SortAIForMainPlayer()
    {
        final PlayerModel player=listPlayer.get(0);
        Array<CardModel> list = new Array<CardModel> ();
        list.addAll (player.listChi.get(0));
        list.addAll (player.listChi.get(1));
        list.addAll (player.listChi.get(2));

        Array<Array<CardModel>> list_result = SortAIMain (list);

        player.listChi = list_result;
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                player.SortBranch ();
            }
        });
        //System.out.println("tong thoi gian AI " +player.index+ ": "+(end-start));

    }

    public void SortAIForPlayer(final PlayerModel player)
    {
        long start=System.currentTimeMillis();
        Array<CardModel> list = new Array<CardModel> ();
        list.addAll (player.listChi.get(0));
        list.addAll (player.listChi.get(1));
        list.addAll (player.listChi.get(2));

        Array<Array<CardModel>> list_result = SortAI (list);

        if (player.isCompletedSort || this.isDestroy)
            return;
        player.listChi = list_result;
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                player.SortBranch ();
                player.SetCompletedSort ();
            }
        });
        long end=System.currentTimeMillis();
        //System.out.println("tong thoi gian AI " +player.index+ ": "+(end-start));

    }
    public void SwapCard(CardModel card_1,CardModel card_2)
    {
        if (card_1 == null || card_2 == null)
            return;
        Array<CardModel> list_1 = new Array<CardModel>();
        Array<CardModel> list_2 = new Array<CardModel>();

        PlayerModel cur_player=listPlayer.get(0);
        for (PlayerModel player : listPlayer) {
            if (player.GetListContainCard (card_1)!=null)
            {
                list_1 = player.GetListContainCard (card_1);
                cur_player = player;
            }
            if (player.GetListContainCard (card_2)!=null)
                list_2 = player.GetListContainCard (card_2);
        }

        if (list_1 == null || list_2 == null)
            return;
        if (list_1.size == 0 || list_2.size == 0)
            return;

        CardModel card_tmp = card_1;
        int index_1 = list_1.indexOf (card_1,false);
        int index_2 = list_2.indexOf (card_2,false);

        if (index_1 < 0 || index_2 < 0)
            return;
        list_1.set(index_1,card_2);
        list_2.set(index_2,card_tmp);

        cur_player.SetSort ();
    }
    boolean IsThung(Array<CardModel> list_tmp)
    {
        if (list_tmp.size != 5)
            return false;
        for (int i=0;i<list_tmp.size-1;i++)
        {
            if (list_tmp.get(i+1).type!=list_tmp.get(i).type)
                return false;
        }
        return true;
    }
    boolean IsSanh(Array<CardModel> list_tmp)
    {
        if (list_tmp.size != 5)
            return false;
        SortCardInList (list_tmp,false); //sort kieu 1->13
        Array<CardModel> list_tmp_2=new Array<CardModel>();
        for (int i = 0; i < list_tmp.size-1; i++) {
            if (list_tmp .get(i + 1).number - list_tmp.get(i).number != 1) {

                list_tmp_2.clear ();
                list_tmp_2.addAll (list_tmp);
                SortCardInListType2 (list_tmp_2,false); //sort kieu 2->1
                for (int j = 0; j < list_tmp_2.size-1; j++) {
                    if (list_tmp_2 .get(j + 1).GetNumberType2() - list_tmp_2 .get(j).GetNumberType2() != 1)
                        return false;
                }
            }
        }
        return true;
    }

    boolean IsXamCo(Array<CardModel> list)
    {
        if (list == null)
            return false;

        if (list.size == 3) {
            for (int i = 0; i < 2; i++)
                if (list .get(i).number != list .get(i + 1).number)
                    return false;
            return true;
        }

        if (list.size != 5)
            return false;

        Array<CardModel> list_tmp = new Array<CardModel> ();
        list_tmp.addAll (list);
        SortCardInList (list_tmp,false);


        int dem = 0;
        CardModel card_tmp = list_tmp .get(0);
        for (int i = 1; i < list_tmp.size; i++) {
            if (list_tmp .get(i).number != card_tmp.number) {
                card_tmp = list_tmp .get(i);
                dem++;
            }
        }
        if (dem != 2)
            return false;
        for (int i = 1; i < list_tmp.size - 1; i++) {
            if (list_tmp .get(i - 1).number == list_tmp .get(i).number && list_tmp .get(i).number == list_tmp .get(i + 1).number)
                return true;
        }
        return false;

    }

    boolean IsThu(Array<CardModel> list)
    {
        if (list.size != 5)
            return false;
        Array<CardModel> list_tmp = new Array<CardModel> ();
        list_tmp.addAll (list);
        SortCardInList (list_tmp,false);


        int dem = 0;
        CardModel card_tmp = list_tmp .get(0);
        for (int i = 1; i < list_tmp.size; i++) {
            if (list_tmp .get(i).number != card_tmp.number) {
                card_tmp = list_tmp .get(i);
                dem++;
            }
        }
        if (dem != 2)
            return false;
        for (int i = 1; i < list_tmp.size - 1; i++) {
            if (list_tmp .get(i - 1).number == list_tmp .get(i).number && list_tmp .get(i).number == list_tmp .get(i + 1).number)
                return false;
        }
        return true;
    }

    boolean IsDoi(Array<CardModel> list)
    {
        if (list.size < 3)
            return false;
        Array<CardModel> list_tmp = new Array<CardModel> ();
        list_tmp.addAll (list);
        SortCardInList (list_tmp,false);

        for (int i = 0; i < list_tmp.size-1; i++) {
            if (list_tmp .get(i + 1).number == list_tmp .get(i).number)
                return true;
        }
        return false;
    }
    public TypeBranch GetTypeBranch(Array<CardModel> list_to_get)
    {
        Array<CardModel> list = new Array<CardModel> (list_to_get);

        MyLinkedMap<Integer,Integer> list_dem=ListDemSoLanXuatHien(list);
        int so_la_giong_nhau = SoLaGiongNhauMax(list_dem);
        int so_cap_giong_nhau=SoCapGiongNhau(list_dem);
        if (so_la_giong_nhau > 1) {
            if (so_la_giong_nhau==4)
                return TypeBranch.TUQUY;
            if (so_la_giong_nhau==3)
            {
                if (so_cap_giong_nhau>1)
                    return TypeBranch.CULU;
                return TypeBranch.XAMCO;
            }
            if (so_cap_giong_nhau>1)
                return TypeBranch.THU;
            return TypeBranch.DOI;
        }

        boolean is_thung = IsThung(list);
        boolean is_sanh = IsSanh(list);
        if (is_sanh&&is_thung)
            return TypeBranch.THUNGPHASANH;
        if (is_sanh)
            return TypeBranch.SANH;
        if (is_thung)
            return TypeBranch.THUNG;
        return TypeBranch.MAUTHAU;
    }

    int SoLaGiongNhauMax(MyLinkedMap<Integer, Integer> list_count_each_number)
    {
        int max = 1;
        for (int i = 0; i < list_count_each_number.size(); i++) {
            int value = list_count_each_number.getValue(i);
            if (value>max)
                max=value;
        }
        return max;
    }
    int SoCapGiongNhau(MyLinkedMap<Integer, Integer> list_count_each_number){
        int dem=0;
        for (int i = 0; i < list_count_each_number.size(); i++) {
            int value = list_count_each_number.getValue(i);
            if (value>1)
                dem++;
        }
        return dem;
    }
    MyLinkedMap<Integer, Integer> ListDemSoLanXuatHien(Array<CardModel> list_card_model) {
        MyLinkedMap<Integer, Integer> list_count_each_number = new MyLinkedMap<Integer, Integer>();
        for (int i = 0; i < list_card_model.size; i++) {
            int num = list_card_model.get(i).number;
            if (!list_count_each_number.containsKey(num))
                list_count_each_number.put(num, 1);
            else
                list_count_each_number.put(num, list_count_each_number.get(num) + 1);
        }
        return list_count_each_number;
    }
    boolean IsSanh3(Array<CardModel> list)
    {
        if (list.size != 3)
            return false;
        SortCardInList (list,false);
        if (list .get(1).number - list .get(0).number != 1 || list .get(2).number - list .get(1).number != 1) {
            SortCardInListType2 (list,false);
            if (list .get(1).GetNumberType2() - list .get(0).GetNumberType2() != 1 || list .get(2).GetNumberType2() - list .get(1).GetNumberType2() != 1)
                return false;
        }
        return true;
    }

    boolean IsThung3(Array<CardModel> list)
    {
        if (list.size != 3)
            return false;
        for (int i = 0; i < 2; i++)
            if (list .get(i).type != list .get(i + 1).type)
                return false;
        return true;
    }


    public int CompareTwoBranch(Array<CardModel> list_1,Array<CardModel> list_2)
    {
        return CompareTwoBranchWithType(list_1,list_2,GetTypeBranch(list_1),GetTypeBranch(list_2));
    }
    int CompareTwoBranchWithType(Array<CardModel> list_1,Array<CardModel> list_2,TypeBranch type1,TypeBranch type2){
        if (type1.ordinal() < type2.ordinal())
            return 1;
        else if (type1.ordinal() > type2.ordinal())
            return -1;

        Array<CardModel> list_tmp_1 = new Array<CardModel> ();
        list_tmp_1.addAll (list_1);
        SortBranchWithType (list_tmp_1,type1);
        Array<CardModel> list_tmp_2 = new Array<CardModel> ();
        list_tmp_2.addAll (list_2);
        SortBranchWithType (list_tmp_2,type2);

        int result = CompareTwoBranchSameType (list_tmp_1, list_tmp_2,type1);
        return result;
    }

    int CompareTwoBranchWhenSortAIWithTypeBranch(Array<CardModel> list_1,Array<CardModel> list_2,int type_s1,int type_s2)
    {
        int s1 = type_s1;
        int s2 = type_s2;
        if (s1 != s2 )
            return (s1*(-1)-9)-(s2*(-1)-9);

        Array<CardModel> list_tmp_1 = new Array<CardModel> (list_1);
        SortBranchWithType(list_tmp_1,TypeBranch.values()[s1]);
        Array<CardModel> list_tmp_2 = new Array<CardModel> (list_2);
        SortBranchWithType (list_tmp_2,TypeBranch.values()[s2]);

        return CompareTwoBranchSameTypeWhenSortAI (list_tmp_1,list_tmp_2,TypeBranch.values()[s1]);
    }

    int CompareTwoBranchSameTypeWhenSortAI(Array<CardModel> list_1,Array<CardModel> list_2,TypeBranch typeBranch)
    {
        TypeBranch type = typeBranch;
        {
            if (type == TypeBranch.TUQUY || type==TypeBranch.CULU) {
                if (list_1 .get(0).number == list_2 .get(0).number) {
                    if (list_1 .get(4).GetNumberType2() > list_2 .get(4).GetNumberType2())
                        return -1;
                    else if (list_1 .get(4).GetNumberType2() < list_2 .get(4).GetNumberType2())
                        return 1;
                    return 0;
                }

            }
            if (type == TypeBranch.THU) {
                if (list_1 .get(0).number == list_2 .get(0).number) {
                    if (list_1 .get(0).GetNumberType2() > list_2 .get(0).GetNumberType2())
                        return 1;
                    else if (list_1 .get(0).GetNumberType2() < list_2 .get(0).GetNumberType2())
                        return -1;

                    if (list_1.get(2).GetNumberType2() > list_2.get(2).GetNumberType2())
                        return -1;
                    else if (list_1.get(2).GetNumberType2() < list_2.get(2).GetNumberType2())
                        return 1;
                    return 0;
                }
            }
        }
        return CompareTwoBranchSameTypeWhenSortAIVersion2 (list_1,list_2,typeBranch);
    }
    public void SortBranchWithType(Array<CardModel> list,TypeBranch typeBranch)
    {
        GameModel.TypeBranch type = typeBranch;
        if (type == GameModel.TypeBranch.THUNGPHASANH || type==TypeBranch.THUNG) {
            SortCardInListType2 (list,true);
            return;
        }
        if (type == GameModel.TypeBranch.SANH) {
            SortCardInList (list,true);
            if (list.get(list.size-2).number != 2)
                SortCardInListType2 (list,true);
            return;
        }
        if (type == TypeBranch.TUQUY) {
            SortCardInList (list,true);
            if (list.get(0).number != list .get(1).number) {
                CardModel card_1 = list .get(0);
                CardModel card_2 = list .get(4);
                SwapTwoCardsInList (list,card_1, card_2);
            }
            return;
        }
        if (type == TypeBranch.CULU) {
            SortCardInListType2 (list, true);
            if (list .get(1).number != list .get(2).number) {
                CardModel card_1 = list .get(0);
                CardModel card_2 = list .get(1);
                list.removeValue(card_1,false);
                list.removeValue (card_2,false);
                list.add(card_1);
                list.add(card_2);
            }
            return;
        }

        if (type == TypeBranch.XAMCO) {
            SortCardInListType2 (list,true);
            for (int i = 1; i < list.size - 1; i++)
                if (list .get(i + 1).number == list .get(i).number && list .get(i - 1).number == list .get(i).number) {
                    Array<CardModel> list_new = new Array<CardModel> ();
                    list_new.add(list.get(i-1));
                    list_new.add(list.get(i));
                    list_new.add(list.get(i+1));

                    DeleteCardByList (list,list_new);

                    Insert(list,list_new,0);
                    return;
                }
        }

        if (type == TypeBranch.THU) {
            SortCardInListType2 (list,true);
            if (list .get(0).number != list .get(1).number) {
                CardModel card = list .get(0);
                list.removeValue(card,false);
                list.add(card);
            } else if (list .get(4).number == list .get(3).number) {
                CardModel card = list .get(2);
                list.removeValue (card,false);
                list.add(card);
            }
            return;
        }

        if (type == TypeBranch.DOI) {
            SortCardInListType2 (list,true);
            for (int i = 0; i < list.size-1; i++) {
                if (list .get(i).number == list .get(i + 1).number) {
                    CardModel card_1 = list .get(i);
                    CardModel card_2 = list.get(i + 1);
                    list.removeValue (card_1,false);
                    list.removeValue (card_2,false);
                    list.insert (0,card_1);
                    list.insert (0,card_2);
                    return;
                }
            }
        }

        if (type == TypeBranch.MAUTHAU) {
            SortCardInListType2 (list,true);
        }
    }
    public void SortBranch(Array<CardModel> list)
    {
        GameModel.TypeBranch type = GetTypeBranch (list);
        if (type == GameModel.TypeBranch.THUNGPHASANH || type==TypeBranch.THUNG) {
            SortCardInListType2 (list,true);
            return;
        }
        if (type == GameModel.TypeBranch.SANH) {
            SortCardInList (list,true);
            if (list.get(list.size-2).number != 2)
                SortCardInListType2 (list,true);
            return;
        }
        if (type == TypeBranch.TUQUY) {
            SortCardInList (list,true);
            if (list.get(0).number != list .get(1).number) {
                CardModel card_1 = list .get(0);
                CardModel card_2 = list .get(4);
                SwapTwoCardsInList (list,card_1, card_2);
            }
            return;
        }
        if (type == TypeBranch.CULU) {
            SortCardInListType2 (list, true);
            if (list .get(1).number != list .get(2).number) {
                CardModel card_1 = list .get(0);
                CardModel card_2 = list .get(1);
                list.removeValue(card_1,false);
                list.removeValue (card_2,false);
                list.add(card_1);
                list.add(card_2);
            }
            return;
        }

        if (type == TypeBranch.XAMCO) {
            SortCardInListType2 (list,true);
            for (int i = 1; i < list.size - 1; i++)
                if (list .get(i + 1).number == list .get(i).number && list .get(i - 1).number == list .get(i).number) {
                    Array<CardModel> list_new = new Array<CardModel> ();
                    list_new.add(list.get(i-1));
                    list_new.add(list.get(i));
                    list_new.add(list.get(i+1));

                    DeleteCardByList (list,list_new);

                    Insert(list,list_new,0);
                    return;
                }
        }

        if (type == TypeBranch.THU) {
            SortCardInListType2 (list,true);
            if (list .get(0).number != list .get(1).number) {
                CardModel card = list .get(0);
                list.removeValue(card,false);
                list.add(card);
            } else if (list .get(4).number == list .get(3).number) {
                CardModel card = list .get(2);
                list.removeValue (card,false);
                list.add(card);
            }
            return;
        }

        if (type == TypeBranch.DOI) {
            SortCardInListType2 (list,true);
            for (int i = 0; i < list.size-1; i++) {
                if (list .get(i).number == list .get(i + 1).number) {
                    CardModel card_1 = list .get(i);
                    CardModel card_2 = list.get(i + 1);
                    list.removeValue (card_1,false);
                    list.removeValue (card_2,false);
                    list.insert (0,card_1);
                    list.insert (0,card_2);
                    return;
                }
            }
        }

        if (type == TypeBranch.MAUTHAU) {
            SortCardInListType2 (list,true);
        }
    }

    int CompareTwoBranchSameType(Array<CardModel> list_1,Array<CardModel> list_2,TypeBranch type)
    {
        int loop = 5;
        if (type == TypeBranch.XAMCO || type == TypeBranch.TUQUY)
            loop = 2;
        for (int i = 0; i < loop; i++) {
            if (i == 3) {
                if (list_1.size == 3 && list_2.size == 3) {
                    if (listHost.contains (list_1 .get(0),false))
                        return 1;
                    if (listHost.contains (list_2 .get(0),false))
                        return -1;
                    return 0;
                }
                if (list_1.size == 3)
                    return -1;
                if (list_2.size == 3)
                    return 1;
            }
            if (list_1 .get(0).number == 1 || list_2.get(0).number==1) {
                if (list_1 .get(i).GetNumberType2() > list_2 .get(i).GetNumberType2())
                    return 1;
                else if (list_1 .get(i).GetNumberType2() < list_2 .get(i).GetNumberType2())
                    return -1;
            } else {
                if (list_1 .get(i).number > list_2 .get(i).number)
                    return 1;
                else if (list_1 .get(i).number < list_2 .get(i).number)
                    return -1;
            }
        }
        if (listHost.contains (list_1 .get(0),false))
            return 1;
        if (listHost.contains (list_2 .get(0),false))
            return -1;
        return 0;
    }

    int CompareTwoBranchSameTypeWhenSortAIVersion2(Array<CardModel> list_1,Array<CardModel> list_2,TypeBranch typeBranch)
    {
        int loop = 5;
        TypeBranch type = typeBranch;
        if (type == TypeBranch.XAMCO || type == TypeBranch.TUQUY)
            loop = 2;
        for (int i = 0; i < loop; i++) {
            if (i == 3) {
                if (list_1.size == 3 && list_2.size == 3) {
                    if (listHost.contains (list_1 .get(0),false))
                        return 1;
                    if (listHost.contains (list_2 .get(0),false))
                        return -1;
                    return 0;
                }
                if (list_1.size == 3)
                    return -1;
                if (list_2.size == 3)
                    return 1;
            }
            if (list_1 .get(0).number == 1 || list_2.get(0).number==1) {
                if (list_1 .get(i).GetNumberType2() > list_2 .get(i).GetNumberType2())
                    return 1;
                else if (list_1 .get(i).GetNumberType2() < list_2 .get(i).GetNumberType2())
                    return -1;
            } else {
                if (list_1 .get(i).number > list_2 .get(i).number)
                    return 1;
                else if (list_1 .get(i).number < list_2 .get(i).number)
                    return -1;
            }
        }
        return 0;
    }
    int ComparelistChi(Array<Array<CardModel>> list_1,Array<Array<CardModel>> list_2)
    {
        if (!CheckValidlistChi (list_1) && !CheckValidlistChi (list_2))
            return 0;
        if (!CheckValidlistChi (list_1))
            return -1;
        if (!CheckValidlistChi (list_2))
            return 1;

        int dem = 0;
        dem += CompareTwoBranch (list_1.get(0),list_2.get(0)) + CompareTwoBranch (list_1.get(1),list_2.get(1)) + CompareTwoBranch (list_1.get(2),list_2.get(2));
        return dem;
    }

    int ComparelistChiWhenSortAI(Array<Array<CardModel>> list_1,Array<Array<CardModel>> list_2)
    {
        if (!CheckValidlistChi (list_1) && !CheckValidlistChi (list_2))
            return 0;
        if (!CheckValidlistChi (list_1))
            return -1;
        if (!CheckValidlistChi (list_2))
            return 1;

        int dem = 0;
        dem += CompareTwoBranchWhenSortAIWithTypeBranch (list_1.get(0),list_2.get(0),GetTypeBranch(list_1.get(0)).ordinal(),GetTypeBranch(list_2.get(0)).ordinal()) + CompareTwoBranchWhenSortAIWithTypeBranch (list_1.get(1),list_2.get(1),GetTypeBranch(list_1.get(1)).ordinal(),GetTypeBranch(list_2.get(1)).ordinal()) + CompareTwoBranchWhenSortAIWithTypeBranch (list_1.get(2),list_2.get(2),GetTypeBranch(list_1.get(2)).ordinal(),GetTypeBranch(list_2.get(2)).ordinal());
        return dem;
    }

    int ComparelistChiWithScore(Array<Array<CardModel>> list_1,Array<Array<CardModel>> list_2)
    {
        int result = ComparelistChiWhenSortAI (list_1,list_2);
        if (result == 0) {
            int count_1 = CountMauThau (list_1);
            int count_2 = CountMauThau (list_2);
            if (count_1 < count_2)
                return 1;
            else if (count_1 > count_2)
                return -1;
        }
        return result;
    }

    int CountMauThau(Array<Array<CardModel>> list)
    {
        int dem = 0;
        for (Array<CardModel> list_card : list)
            if (GetTypeBranch (list_card) == TypeBranch.MAUTHAU)
                dem++;
        return dem;
    }

    Array<Array<CardModel>> SortAI(Array<CardModel> list_to_sort)
    {
        Array<Array<CardModel>> list_result = new Array<Array<CardModel>> ();
        Array<Array<CardModel>> list_result_tmp = new Array<Array<CardModel>> ();
        int iDem = 0;
        while (iDem < 3) {
            list_result.add(new Array<CardModel>());
            list_result_tmp.add(new Array<CardModel>());
            iDem++;
        }
        Array<CardModel> list_tmp = new Array<CardModel> ();


        for (int i = 0; i < 9; i++) {
            for (int j = i + 1; j < 10; j++) {
                for (int k = j + 1; k < 11; k++) {
                    for (int l = k + 1; l < 12; l++) {
                        for (int m = l + 1; m < 13; m++) {

                            list_tmp.clear ();
                            list_tmp.addAll (list_to_sort);

                            list_result.get(0).clear();

                            list_result.get(0).add(list_tmp.get(i));
                            list_result.get(0).add(list_tmp.get(j));
                            list_result.get(0).add(list_tmp.get(k));
                            list_result.get(0).add(list_tmp.get(l));
                            list_result.get(0).add(list_tmp.get(m));

                            TypeBranch type_1=GetTypeBranch(list_result .get(0));
                            if (type_1 == TypeBranch.MAUTHAU)
                             continue;
                            list_tmp.removeAll(list_result.get(0),false);

                            TypeBranch type_2=GetTypeBranch(list_result_tmp .get(0));

                            if (list_result_tmp .get(0).size > 0) {
                                if (CompareTwoBranchWhenSortAIWithTypeBranch (list_result .get(0), list_result_tmp .get(0),type_1.ordinal(),type_2.ordinal()) < 0)
                                    continue;
                            }


                            for (int n = 0; n < 4; n++) {
                                for (int o = n+1; o < 5; o++) {
                                    for (int p = o+1; p < 6; p++) {
                                        list_result .get(2).clear ();
                                        list_result.get(2).add(list_tmp.get(n));
                                        list_result.get(2).add(list_tmp.get(o));
                                        list_result.get(2).add(list_tmp.get(p));

                                        Array<CardModel> list_new = new Array<CardModel> ();
                                        list_new.addAll (list_tmp);

                                        DeleteCardByList (list_new, list_result.get(2));

                                        list_result.get(1).clear();
                                        list_result .get(1).addAll (list_new);

                                        if (!CheckValidlistChi (list_result))
                                            continue;
                                        if (list_result_tmp .get(0).size == 0 || ComparelistChiWithScore (list_result, list_result_tmp) > 0) {

                                            list_result_tmp .get(0).clear();
                                            list_result_tmp .get(0).addAll (list_result.get(0));
                                            list_result_tmp .get(1).clear();
                                            list_result_tmp .get(1).addAll (list_result.get(1));
                                            list_result_tmp .get(2).clear();
                                            list_result_tmp .get(2).addAll (list_result.get(2));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return list_result_tmp;
    }

    Array<Array<CardModel>> SortAIMain(Array<CardModel> list_to_sort)
    {
        Array<Array<CardModel>> list_result = new Array<Array<CardModel>> ();
        Array<Array<CardModel>> list_result_tmp = new Array<Array<CardModel>> ();
        int iDem = 0;
        while (iDem < 3) {
            list_result.add(new Array<CardModel>());
            list_result_tmp.add(new Array<CardModel>());
            iDem++;
        }
        Array<CardModel> list_tmp = new Array<CardModel> ();


        for (int i = 0; i < 9; i++) {
            for (int j = i + 1; j < 10; j++) {
                for (int k = j + 1; k < 11; k++) {
                    for (int l = k + 1; l < 12; l++) {
                        for (int m = l + 1; m < 13; m++) {

                            list_tmp.clear ();
                            list_tmp.addAll (list_to_sort);

                            list_result.get(0).clear();

                            list_result.get(0).add(list_tmp.get(i));
                            list_result.get(0).add(list_tmp.get(j));
                            list_result.get(0).add(list_tmp.get(k));
                            list_result.get(0).add(list_tmp.get(l));
                            list_result.get(0).add(list_tmp.get(m));

                            TypeBranch type_1=GetTypeBranch(list_result .get(0));
                            if (type_1 == TypeBranch.MAUTHAU)
                                continue;
                            list_tmp.removeAll(list_result.get(0),false);

                            TypeBranch type_2=GetTypeBranch(list_result_tmp .get(0));

                            if (list_result_tmp .get(0).size > 0) {
                                if (CompareTwoBranchWhenSortAIWithTypeBranch (list_result .get(0), list_result_tmp .get(0),type_1.ordinal(),type_2.ordinal()) < 0)
                                    continue;
                            }


                            for (int n = 0; n < 4; n++) {
                                for (int o = n+1; o < 5; o++) {
                                    for (int p = o+1; p < 6; p++) {
                                        list_result .get(2).clear ();
                                        list_result.get(2).add(list_tmp.get(n));
                                        list_result.get(2).add(list_tmp.get(o));
                                        list_result.get(2).add(list_tmp.get(p));

                                        Array<CardModel> list_new = new Array<CardModel> ();
                                        list_new.addAll (list_tmp);

                                        DeleteCardByList (list_new, list_result.get(2));

                                        list_result.get(1).clear();
                                        list_result .get(1).addAll (list_new);

                                        if (!CheckValidlistChi (list_result))
                                            continue;
                                        return list_result;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return list_result_tmp;
    }
    public boolean CheckValidlistChi(Array<Array<CardModel>> list)
    {
        return CompareTwoBranch (list .get(0), list .get(1)) > -1 && CompareTwoBranch (list .get(1), list .get(2)) > -1;
    }

    void CountScoreOfTwoPlayers(PlayerModel player_1,PlayerModel player_2,int index_branch)
    {

        int scoreWin=0;
        if (!IsPlayerExist (player_1) || !IsPlayerExist (player_2)) {
            player_1.AddResult (player_2,0,scoreWin);
            player_2.AddResult (player_1,0,scoreWin);
            return;
        }
        if (!CheckValidlistChi (player_1.listChi) || !CheckValidlistChi (player_2.listChi)) {
            player_1.AddResult (player_2,0,scoreWin);
            player_2.AddResult (player_1,0,scoreWin);
            return;
        }


        int result = (CompareTwoBranch (player_1.listChi.get(index_branch), player_2.listChi .get(index_branch)));
        switch (result) {
            case 0:
                player_1.AddResult (player_2,0,scoreWin);
                player_2.AddResult (player_1,0,scoreWin);
                break;
            case 1:
                scoreWin = GetScoreWinOfBranch (player_1.listChi .get(index_branch), index_branch);
                player_1.AddResult (player_2,1,scoreWin);
                player_2.AddResult (player_1,-1,scoreWin);
                break;
            case -1:
                scoreWin = GetScoreWinOfBranch (player_2.listChi .get(index_branch), index_branch);
                player_1.AddResult (player_2,-1,scoreWin);
                player_2.AddResult (player_1,1,scoreWin);
                break;
            default:
                break;
        }
    }

    boolean IsPlayerExist(PlayerModel player)
    {
        return GetListPlayerExist ().contains (player,false);
    }

    public int GetScoreWinOfBranch(Array<CardModel> list, int index_branch)
    {
        if (GetTypeBranch (list) == TypeBranch.THUNGPHASANH) {
            if (index_branch == 0)
                return scoreBet * 10;
            if (index_branch == 1)
                return scoreBet * 20;
        }
        if (GetTypeBranch (list) == TypeBranch.TUQUY) {
            Array<CardModel> list_tmp = new Array<CardModel>();
            list_tmp.addAll(list);
            SortBranch (list_tmp);
            if (list_tmp .get(0).number == 1 && GameModel.Instance.isAce)
                return scoreBet * 20;
            if (index_branch == 0)
                return scoreBet * 8;
            if (index_branch == 1)
                return scoreBet * 16;
        }
        if (GetTypeBranch (list) == TypeBranch.CULU && index_branch == 1)
            return scoreBet * 4;
        if (GetTypeBranch (list) == TypeBranch.XAMCO && index_branch == 2) {
            if (IsHaveNumberInList(list,1) && GameModel.Instance.isAce)
                return scoreBet * 20;
            return scoreBet * 6;
        }
        return scoreBet;
    }

    boolean IsHaveNumberInList(Array<CardModel> list,int number)
    {
        for (CardModel card : list)
            if (card.number == number)
                return true;
        return false;
    }

    boolean IsHaveWinAbsolute()
    {	for (PlayerModel player : listPlayer) {
        if (player.typeWinAbsolute != TypeWinAbsolute.KHONGANTRANG)
            return true;
    }
        return false;
    }

    void SetWinAbsolute()
    {
        for (int i=0;i<listPlayer.size;i++) {
            PlayerModel player=listPlayer.get(i);
            if (player.typeWinAbsolute == TypeWinAbsolute.KHONGANTRANG)
                continue;
            player.SetWinAbsoluteWithAllPlayers ();
            RemovePlayer (player);
        }
    }

    public void CountScore(){
        listHost.clear();
        CountScoreTimer();
    }
    public void CountScoreTimer() {

        DongBai();
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                ResetListResult();
                KiemTraWinAbsolute();
            }
        },0.5f);
    }
    void DongBai(){
        GameController.Instance.ClearReportPlayerViews();
        GameController.Instance.ClearColorReportPlayerViews();
        UIController.Instance.playingUI.SetCountScore(false);
        if (this.isDestroy)
            return;
//       // Gdx.app.log("ThreadCountScore","running");
//        Gdx.app.log("CountScore","");
        //yield return new WaitForSeconds (0.5f);
        for (PlayerModel player : listPlayer) {
            player.SortBranch();
            player.SetLockTypeBranch();
            for (int i = 0; i < 3; i++) {
                player.SetShadowForBranch(i, 0);
            }
        }

        for (PlayerModel player : listPlayer) {
//			SoundController.PlayClip (SoundController.Instance.clipShowBranch);
            player.SetOpenAllBranch(false); //test lat. bai
        }
    }
    void ResetListResult(){
        for (int i = 0; i < listPlayer.size; i++)
            listPlayer.get(i).ResetListResult();
    }
    void KiemTraWinAbsolute(){
        if (IsHaveWinAbsolute()) {
            SetWinAbsolute();

            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    for (PlayerModel player : listPlayer) {
                        player.SetChangeScoreSummaryView();
                    }
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            for (PlayerModel player : listPlayer) {
                                if (listPlayerExists.contains(player, false))
                                    continue;
                                for (int i = 0; i < 3; i++)
                                    player.SetShadowForBranch(i, 1);
                                KiemTraBinhLung();
                            }
                        }
                    },2.5f);
                }
            },2f);


        }
        else
            KiemTraBinhLung();
    }
    void KiemTraBinhLung(){
        if (IsHaveBinhLung ()) {
            SetBinhLung ();
            SoundController.Instance.PlayClipEatCard();
            System.out.print("eat");
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    TinhTungChi();
                }
            },1.5f);
        }
        else
            TinhTungChi();
    }
    void TinhTungChi(){
        for (PlayerModel player : listPlayerExists)
            player.SetTypeBranch (true);
        if (listPlayerExists.size > 1) { //tinh tung chi
            int i=0;
            while (i<3) {
                final int finalI = i;
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        for (int j=0;j<listPlayerExists.size;j++) {
                            PlayerModel player=listPlayerExists.get(j);
                            //player.SetUnShadowForBranch (i);
                            player.SetFlipBranch (finalI);
                            player.SetShowTypeBranch (finalI);
                            player.SetPriorityBranch (finalI,true);
                        }
                        SetCompareBranch (finalI);
                        SoundController.Instance.PlayClipDownCards();
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                GameController.Instance.HideTextCountScore();
                                for (int j=0;j<listPlayerExists.size;j++) {
                                    PlayerModel player_1=listPlayerExists.get(j);
                                    for (int k=0;k<listPlayerExists.size;k++) {
                                        PlayerModel player_2=listPlayerExists.get(k);
                                        if (player_1 == player_2)
                                            continue;
                                        if (player_1.IsCompleteCount (finalI) || player_2.IsCompleteCount (finalI))
                                            continue;
                                        CountScoreOfTwoPlayers (player_1,player_2, finalI);
                                    }
                                }
                                for (int j=0;j<listPlayerExists.size;j++) {
                                    PlayerModel player=listPlayerExists.get(j);
                                    player.ChangeScore (finalI);
                                }
                                SoundController.Instance.PlayClipCoin();

                            }
                        },1.5f);
                        Timer.schedule(new Timer.Task() {
                            @Override
                            public void run() {
                                for (int j=0;j<listPlayerExists.size;j++) {
                                    PlayerModel player=listPlayerExists.get(j);
                                    player.SetShadowForBranch (finalI,1);
                                    player.SetLockTypeBranch ();
                                    player.SetPriorityBranch (finalI,false);
                                }
                            }
                        },2.5f);
                    }
                },4*i);
                i++;
            }
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    for (int i=0;i<listPlayer.size;i++)
                    {
                        PlayerModel player = listPlayer.get(i);
                        player.SetUpdateListHoldNotShadow();
                    }
                    KiemTraSap3Chi();
                }
            },12f);
        }
        else {
            for (int i=0;i<listPlayer.size;i++)
            {
                PlayerModel player = listPlayer.get(i);
                player.SetUpdateListHoldNotShadow();
            }
            LamToiBai();
        }

    }
    void KiemTraSap3Chi(){
        if (CheckLoseThreeBranch ()) {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    SoundController.Instance.PlayClipEatCard();
                    SetLoseThreeBranch ();
                    for(int i=0;i<listPlayerExists.size;i++)
                    {
                        PlayerModel player=listPlayerExists.get(i);
                        if (player.isLoseThreeBranch)
                            player.SetReportSap3Chi ();
                        if (player.scoreToCount > 0) {
                            for (int j = 0; j < 3; j++)
                                player.SetUnShadowForBranch (j);
                        }
                    }

                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {

                            for (int i=0;i<listPlayerExists.size;i++) {
                                PlayerModel player=listPlayerExists.get(i);
                                if (player.scoreToCount!=0 || player.isLoseThreeBranch)
                                    player.SetChangeScore (player.scoreToCount);
                            }
                            SoundController.Instance.PlayClipCoin();

                            Timer.schedule(new Timer.Task() {
                                @Override
                                public void run() {
                                    KiemTraSapLang();
                                }
                            },1f);
                        }
                    },1.5f);
                }
            },1f);
        }
        else
            KiemTraSapLang();
    }
    void KiemTraSapLang(){
        final PlayerModel player_win_all = GetPlayerWinAll (); // player win all
        if (player_win_all != null) {
            SoundController.Instance.PlayClipPlayCard();
            SetWinAll(player_win_all);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {

                    player_win_all.SetWinAll();
                    UIController.Instance.fireWorkUI.ShowFireWork();
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            LamToiBai();
                        }
                    },1.5f);
                }
            },2.5f);
        }
        else
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    LamToiBai();
                }
            },1.5f);
    }
    void LamToiBai(){
        for (int i=0;i<listPlayer.size;i++) {
            PlayerModel player=listPlayer.get(i);
            for (int j = 0; j < 3; j++) {
                player.SetShadowForBranch(j, 1);
                player.SetOpenBranch(j, true);
            }
        }
        GameController.Instance.ClearReportPlayerViews();
        KiemTraTinhAt();
    }
    void KiemTraTinhAt(){
        if (isAce) {
            CountAce();
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    SoundController.Instance.PlayClipPlayCard();
                    SetCountAce();
                    for (int i = 0; i < listPlayer.size; i++) {
                        PlayerModel player = listPlayer.get(i);
                        player.SetUnShadowCardsAce(true);
                    }
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            for(int i=0;i<listPlayer.size;i++)
                            {
                                PlayerModel player=listPlayer.get(i);
                                player.SetChangeScore (player.scoreToCount);
                            }
                            SoundController.Instance.PlayClipCoin();
                            Timer.schedule(new Timer.Task() {
                                @Override
                                public void run() {
                                    for (int i = 0; i < listPlayer.size; i++) {
                                        PlayerModel player = listPlayer.get(i);
                                        player.SetUnShadowCardsAce(false);
                                    }
                                    Timer.schedule(new Timer.Task() {
                                        @Override
                                        public void run() {
                                            LamSangBaiVaHienTongKet();
                                        }
                                    },1f);
                                }
                            },2f);
                        }
                    },1.5f);
                }
            },0.5f);
        }
        else
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    LamSangBaiVaHienTongKet();
                }
            },0.5f);
    }
    void LamSangBaiVaHienTongKet(){
        for (int i=0;i<listPlayer.size;i++)
        {
            PlayerModel player = listPlayer.get(i);
            player.SetUpdateListHoldNotShadow();
        }
        ShowSummaryScore();
        for (int i=0;i<listPlayer.size;i++) {
            PlayerModel player = listPlayer.get(i);
            for (int j = 0; j < 3; j++) {
                player.SetUnShadowForBranch(i);
                // player.SetPriorityBranch(j, false);
            }
        }
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                TongKet();
            }
        },1.f);
    }
    void TongKet(){
        for (int i=0;i<listPlayer.size;i++)
        {
            PlayerModel player=listPlayer.get(i);
            player.SetScoreSummary();
            player.SetWinOrLose();
        }
        if (GetMainPlayer().scoreChange<0)
            SoundController.Instance.PlayClipLose();
        else{
            SoundController.Instance.PlayClipWin();
            UIController.Instance.fireWorkUI.ShowFireWork();
        }

        gameState = GameState.OVER;
        playState=PlayState.TINHTIENXONG;

        DataModel.Instance.SaveGameState();
        DataModel.Instance.SavePlayState();

        PreferenceController.Instance.preferences.putInteger("IsPlaying", 0);
        UIController.Instance.OnEndCountScore();
        GameController.Instance.OnEndGame();
        is_end=true;
        isCounting = false;
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                HienQuangCaoVaCacNut();
            }
        },2f);
    }
    void HienQuangCaoVaCacNut(){
        MauBinh.zenSDK.ShowFullscreen();
        gameCount++;
        MauBinh.zenSDK.TrackLevelCompleted(gameCount);
        UIController.Instance.playingUI.btnVanMoi.setVisible(true);
        UIController.Instance.playingUI.btnXemLai.setVisible(true);
    }
    public interface OnShowSummaryScore{
        public void OnEvent();
    };
    OnShowSummaryScore cbShowSummaryScore;
    void ShowSummaryScore()
    {
        if (cbShowSummaryScore != null)
            cbShowSummaryScore.OnEvent();
    }
    public void RegisterShowSummaryScore(OnShowSummaryScore cb)
    {
        cbShowSummaryScore =cb;
    }

    public interface OnShowLoseThreeBranch{
        public void OnEvent();
    };
    OnShowLoseThreeBranch cbShowLoseThreeBranch;
    void ShowLoseThreeBranch()
    {
        if (cbShowLoseThreeBranch != null)
            cbShowLoseThreeBranch.OnEvent();
    }
    public void RegisterShowLoseThreeBranch(OnShowLoseThreeBranch cb)
    {
        cbShowLoseThreeBranch =cb;
    }

    public interface OnCountAce{
        public void OnEvent();
    };
    OnCountAce cbCountAce;
    void CountAce()
    {
        if (cbCountAce != null)
            cbCountAce.OnEvent();
    }
    public void RegisterCountAce(OnCountAce cb)
    {
        cbCountAce =cb;
    }
    public interface OnCompareBranch{
        public void OnEvent(int i);
    };
    OnCompareBranch cbCompareBranch;


    void SetCompareBranch(int index_branch)
    {
        if (cbCompareBranch != null)
            cbCompareBranch.OnEvent (index_branch);
    }
    public void RegisterCompareBranch(OnCompareBranch cb)
    {
        cbCompareBranch =cb;
    }


    int CountNumberOfList(Array<CardModel> list)
    {
        SortCardInList (list,false);
        int dem = 1;
        CardModel card_tmp = list .get(0);
        for (int i = 1; i < list.size; i++)
            if (list .get(i).number != card_tmp.number) {
                card_tmp = list .get(i);
                dem++;
            }
        return dem;
    }


    boolean IsHavePair(Array<CardModel> list,CardModel card_model)
    {
        int dem = 0;
        for(CardModel card : list)
        {
            if (card == card_model)
                continue;
            if (card.number == card_model.number)
                return true;
        }
        return false;
    }

    void DeletePairCardInListByNumber(Array<CardModel> list, int number)
    {
        int dem = 0;
        for (int i = 0; i < list.size; i++) {
            if (list .get(i).number == number) {
                list.removeIndex (i);
                i--;
                dem++;
                if (dem == 2)
                    return;
            }
        }
    }

    boolean CheckSanhRong(Array<CardModel> list)
    {
        int count_number = CountNumberOfList (list);
        if (count_number == 13)
            return true;
        return false;
    }

    boolean CheckRongLoc(Array<CardModel> list)
    {
        if (!CheckSanhRong (list))
            return false;

        for (int i = 0; i < list.size-1; i++)
            if (list .get(i).type != list.get(i+1).type) {
                return false;
            }
        return true;
    }

    boolean CheckLucPheV2(Array<CardModel> list)
    {
        Array<CardModel> list_test = new Array<CardModel> ();
        list_test.addAll (list);
        SortCardInList (list_test,false);
        for (int i = 0; i < list_test.size; i++) {
            if (IsHavePair (list_test, list_test .get(i))) {
                DeletePairCardInListByNumber (list_test,list_test.get(i).number);
                i--;
            }
        }
        if (list_test.size == 1)
            return true;
        return false;
    }

    boolean CheckLucPhe(Array<Array<CardModel>> list)
    {
        Array<Array<CardModel>> list_branch = new Array<Array<CardModel>> ();

        while (list_branch.size<3)
        {
            list_branch.add(new Array<CardModel> ());
        }
        for (int i = 0; i < 3; i++) {
            list_branch.get(i).addAll (list.get(i));
        }


        SortBranch (list_branch.get(0));
        SortBranch (list_branch.get(1));
        SortBranch (list_branch.get(2));

        if (GetTypeBranch (list_branch .get(0)) != TypeBranch.TUQUY && GetTypeBranch (list_branch .get(0)) != TypeBranch.THU)
            return false;
        Array<CardModel> list_rac = new Array<CardModel> ();
        list_rac.add(list_branch.get(0).get(4));
        if (GetTypeBranch (list_branch .get(1)) != TypeBranch.TUQUY && GetTypeBranch (list_branch .get(1)) != TypeBranch.THU)
            return false;
        list_rac.add(list_branch.get(1).get(4));
        if (GetTypeBranch (list_branch .get(2)) != TypeBranch.DOI)
            return false;
        list_rac.add(list_branch.get(2).get(2));
        SortCardInList (list_rac,false);
        if (list_rac .get(0).number != list_rac .get(1).number && list_rac .get(1).number != list_rac .get(2).number)
            return false;
        Array<CardModel> list_next = new Array<CardModel> ();
        list_next.addAll (list.get(0));
        list_next.addAll (list.get(1));
        list_next.addAll (list.get(2));
        return CheckLucPheV2(list_next);
    }

    boolean Check5Doi1Xam(Array<Array<CardModel>> list)
    {
        Array<Array<CardModel>> list_branch = new Array<Array<CardModel>> ();

        while (list_branch.size<3)
        {
            list_branch.add(new Array<CardModel> ());
        }
        for (int i = 0; i < 3; i++) {
            list_branch.get(i).addAll (list.get(i));
        }


        SortBranch (list_branch.get(0));
        SortBranch (list_branch.get(1));
        SortBranch (list_branch.get(2));



        Array<CardModel> list_rac = new Array<CardModel> ();
        if (GetTypeBranch(list_branch.get(0))==GameModel.TypeBranch.TUQUY || GetTypeBranch(list_branch.get(0))==GameModel.TypeBranch.THU)
            list_rac.add(list_branch.get(0).get(4));
        if (GetTypeBranch (list_branch .get(1)) != TypeBranch.TUQUY || GetTypeBranch (list_branch .get(1)) != TypeBranch.THU)
            list_rac.add(list_branch.get(1).get(4));
        if (GetTypeBranch (list_branch .get(2)) == TypeBranch.DOI)
            list_rac.add(list_branch.get(2).get(2));
        SortCardInList (list_rac,false);

        if (list_rac.size < 2)
            return false;
        for (int i = 1; i < list_rac.size; i++) {
            if (list_rac .get(i).number != list_rac .get(0).number)
                return false;
        }

        Array<CardModel> list_next = new Array<CardModel> ();
        list_next.addAll (list.get(0));
        list_next.addAll (list.get(1));
        list_next.addAll (list.get(2));
        return Check5Doi1XamV2(list_next);
    }

    boolean CheckCungMau12(Array<CardModel> list)
    {
        CardModel card_tmp = list .get(0);
        Array<Integer> list_color = new Array<Integer>();
        if (card_tmp.type.ordinal()<=1) {
            list_color.add(0);
            list_color.add(1);
        } else {
            list_color.add(2);
            list_color.add(3);
        }
        int dem = 0;
        for (int i = 1; i < list.size; i++) {
            if (!list_color.contains (list .get(i).type.ordinal(),false)) {
                dem++;
                if (dem >= 2)
                    return false;
            }
        }
        return true;
    }

    boolean CheckCungMau13(Array<CardModel> list)
    {
        CardModel card_tmp = list .get(0);
        Array<Integer> list_color = new Array<Integer>();
        if ((int)card_tmp.type.ordinal()<=1) {
            list_color.add(0);
            list_color.add(1);
        } else {
            list_color.add(2);
            list_color.add(3);
        }
        int dem = 0;
        for (int i = 1; i < list.size; i++) {
            if (!list_color.contains ((int)list .get(i).type.ordinal(),false)) {
                return false;
            }
        }
        return true;
    }

    boolean Check3Thung(Array<Array<CardModel>> list_branch)
    {
        if (GetTypeBranch (list_branch .get(0)) != TypeBranch.THUNG && GetTypeBranch (list_branch .get(0)) != TypeBranch.THUNGPHASANH)
            return false;
        if (GetTypeBranch (list_branch .get(1)) != TypeBranch.THUNG && GetTypeBranch (list_branch .get(1)) != TypeBranch.THUNGPHASANH)
            return false;
        if (!IsThung3 (list_branch .get(2)))
            return false;
        return true;
    }

    boolean Check3Sanh(Array<Array<CardModel>> list_branch)
    {
        if (GetTypeBranch (list_branch .get(0)) != TypeBranch.SANH && GetTypeBranch (list_branch .get(0)) != TypeBranch.THUNGPHASANH)
            return false;
        if (GetTypeBranch (list_branch .get(1)) != TypeBranch.SANH && GetTypeBranch (list_branch .get(1)) != TypeBranch.THUNGPHASANH)
            return false;
        if (!IsSanh3 (list_branch .get(2)))
            return false;
        return true;
    }

    boolean Check5Doi1XamV2(Array<CardModel> list)
    {
        if (!CheckLucPheV2 (list))
            return false;
        for (int i = 1; i < list.size - 1; i++) {
            if (list .get(i - 1).number == list .get(i).number && list .get(i).number == list .get(i + 1).number)
                return true;
        }
        return false;
    }

    public void CheckWinAbsolute(PlayerModel player)
    {
        if (player.typeWinAbsolute != TypeWinAbsolute.KHONGANTRANG)
            return;
        Array<CardModel> list = new Array<CardModel> ();
        list.addAll (player.listChi.get(0));
        list.addAll (player.listChi.get(1));
        list.addAll (player.listChi.get(2));

        if (CheckRongLoc (list)) {
            player.SetWinAbsolute (TypeWinAbsolute.RONGLOC);
            return;
        }
        if (CheckSanhRong (list)) {
            player.SetWinAbsolute (TypeWinAbsolute.SANHRONG);
            return;
        }
        if (CheckCungMau13 (list)) {
            player.SetWinAbsolute (TypeWinAbsolute.CUNGMAU13);
            return;
        }
        if (CheckCungMau12 (list)) {
            player.SetWinAbsolute (TypeWinAbsolute.CUNGMAU12);
            return;
        }

        Array<Array<CardModel>> list_tmp = new Array<Array<CardModel>>();
        for (int i=0;i<3;i++){
            list_tmp.add(new Array<CardModel>());
            list_tmp.get(i).addAll(player.listChi.get(i));
        }

        if (Check3Thung (list_tmp)) {
            player.SetWinAbsolute (TypeWinAbsolute.BATHUNG);
            return;
        }
        if (Check3Sanh (list_tmp)) {
            player.SetWinAbsolute (TypeWinAbsolute.BASANH);
            return;
        }
        if (CheckLucPhe (list_tmp)) {
            player.SetWinAbsolute (TypeWinAbsolute.LUCPHE);
            return;
        }

		/*
		UnityThread.executeInUpdate (()=>{
			Utilities.DeleteChild (GameController.Instance.txtCompareBranch.gameObject);
		});
*/
    }

    public enum TypeWinAbsolute{KHONGANTRANG,RONGLOC,SANHRONG,NAMDOIMOTXAM,LUCPHE,BATHUNG,BASANH,CUNGMAU12,CUNGMAU13};

    public Array<CardModel> GetListRac(Array<CardModel> list)
    {
        SortBranch (list);

        Array<CardModel> list_rac = new Array<CardModel> ();

        if (GetTypeBranch (list) == TypeBranch.TUQUY) {
            list_rac.add(list.get(4));
            return list_rac;
        }
        if (GetTypeBranch (list) == TypeBranch.XAMCO) {
            if (list.size == 3)
                return list_rac;
            list_rac.add(list.get(3));
            list_rac.add(list.get(4));
            return list_rac;
        }
        if (GetTypeBranch (list) == GameModel.TypeBranch.THU) {
            list_rac.add(list.get(4));
            return list_rac;
        }
        if (GetTypeBranch (list) == GameModel.TypeBranch.DOI) {
            for (int i = 2; i < list.size; i++)
                list_rac.add(list.get(i));
            return list_rac;
        }
        if (GetTypeBranch (list) == GameModel.TypeBranch.MAUTHAU) {
            list_rac.addAll (list);
            return list_rac;
        }
        return list_rac;
    }

    public Array<PlayerModel> GetListPlayerExist() //nhung player ko an trang, ko lung
    {
        return listPlayerExists;
    }

    boolean CheckLoseThreeBranch()
    {
        Array<PlayerModel> listPlayerExists = GetListPlayerExist ();
        for(PlayerModel player : listPlayerExists)
        {
            player.scoreToCount = 0;
        }
        for(PlayerModel player : listPlayerExists)
        {
            player.SetPlayerLoseBranch ();
        }
        for(PlayerModel player : listPlayerExists)
        {
            if (player.scoreToCount!=0)
                return true;
        }
        return false;
    }

    void SetLoseThreeBranch()
    {
        Array<PlayerModel> listPlayerExists = GetListPlayerExist ();
        if (cbShowLoseThreeBranch!=null)
            cbShowLoseThreeBranch.OnEvent();
    }

    PlayerModel GetPlayerWinAll()
    {
        if (listPlayerExists.size <= 2)
            return null;
        for (PlayerModel player : listPlayer) {
            if (player.IsWinAll ())
                return player;
        }
        return null;
    }

    void SetWinAll(PlayerModel player)
    {
        if (cbSetWinAll != null)
            cbSetWinAll.OnEvent();
        for (PlayerModel player_model : listPlayerExists) {
            for (int i = 0; i < 3; i++) {
                if (player_model==player)
                    player_model.SetUnShadowForBranch (i);
                else
                    player_model.SetShadowForBranch (i,1);
            }
        }

    }
    public interface  OnSetWinAll{
        public void OnEvent();
    }
    OnSetWinAll cbSetWinAll;
    public void ResgisterSetWinAll(OnSetWinAll cb)
    {
        cbSetWinAll =cb;
    }

    public int GetScoreWhenWinAbsolute(TypeWinAbsolute type)
    {
        if (type == TypeWinAbsolute.RONGLOC) {
            return scoreBet * 72;
        }
        if (type == TypeWinAbsolute.SANHRONG) {
            return scoreBet * 36;
        }
        if (type == TypeWinAbsolute.NAMDOIMOTXAM) {
            return scoreBet * 36;
        }
        if (type == TypeWinAbsolute.LUCPHE) {
            return scoreBet * 18;
        }
        if (type == TypeWinAbsolute.BATHUNG) {
            return scoreBet * 18;
        }
        if (type == TypeWinAbsolute.BASANH) {
            return scoreBet * 18;
        }
        if (type == TypeWinAbsolute.CUNGMAU13) {
            return scoreBet * 30;
        }
        if (type == TypeWinAbsolute.CUNGMAU12) {
            return scoreBet * 24;
        }
        return 0;
    }

    boolean IsHaveBinhLung()
    {
        for (PlayerModel player : listPlayerExists)
            if (!CheckValidlistChi (player.listChi))
                return true;
        return false;
    }
    Array<PlayerModel> listBinhLung;
    void SetBinhLung()
    {
        for(PlayerModel player : listPlayerExists)
        {
            player.scoreToCount = 0;
        }
        Array<PlayerModel> listValid = new Array<PlayerModel> ();
        Array<PlayerModel> listInvalid = new Array<PlayerModel> ();
        for (PlayerModel player : listPlayerExists) {
            if (!CheckValidlistChi (player.listChi))
                listInvalid.add(player);
            else
                listValid.add(player);
        }
        for (PlayerModel player : listInvalid) {
            player.SetBinhLung (listValid);
        }
        for (PlayerModel player : listPlayerExists) {
            player.scoreChange += player.scoreToCount;
        }
        for (PlayerModel player : listInvalid) {
            RemovePlayer (player);
        }
    }

    void RemovePlayer(PlayerModel player)
    {
        listPlayerExists.removeValue (player,false);
        player.OnRemove ();
    }

    public int GetSumAce()
    {
        int dem = 0;
        for (PlayerModel player : listPlayer) {
            for (Array<CardModel> list : player.listChi) {
                for (CardModel card : list)
                    if (card.number == 1)
                        dem++;
            }
        }
        return dem;
    }

    void SetCountAce()
    {
        for(int i=0;i<listPlayer.size;i++)
        {
            PlayerModel player=listPlayer.get(i);
            player.scoreToCount = 0;
        }
        for(int i=0;i<listPlayer.size;i++) {
            PlayerModel player = listPlayer.get(i);
            player.SetCountAce();
        }
    }
    boolean isCounting;
    public void CheckAllPlayerCompletedSort()
    {
        if (this.isDestroy)
            return;
        for (int i=0;i<listPlayer.size;i++)
            if (!listPlayer.get(i).isCompletedSort)
                return;
        if (isCounting)
            return;

        GameModel.Instance.playState= GameModel.PlayState.TINHTIEN;
        DataModel.Instance.SavePlayState();
        DataModel.Instance.SaveCards();
        DataModel.Instance.SaveAnTrang();

        isCounting = true;
        CountScore();
        GameController.Instance.DisableSubTime();
    }

    public void TestCompareList()
    {
        Array<CardModel> list1 = new Array<CardModel> ();

        list1.add(new CardModel(10,CardModel.CardType.RO));
        list1.add(new CardModel(10,CardModel.CardType.CHUON));
        list1.add(new CardModel(9,CardModel.CardType.CHUON));
        list1.add(new CardModel(8,CardModel.CardType.CO));
        list1.add(new CardModel(7,CardModel.CardType.CO));
        list1.add(new CardModel(6,CardModel.CardType.RO));
        list1.add(new CardModel(5,CardModel.CardType.RO));
        list1.add(new CardModel(4,CardModel.CardType.CO));
        list1.add(new CardModel(4,CardModel.CardType.BICH));
        list1.add(new CardModel(9,CardModel.CardType.CO));
        list1.add(new CardModel(2,CardModel.CardType.BICH));
        list1.add(new CardModel(2,CardModel.CardType.RO));
        list1.add(new CardModel(12,CardModel.CardType.CHUON));

        Array<Array<CardModel>> list_result = SortAI (list1);
        for (Array<CardModel> array:list_result)
        {
            for (CardModel card:array)
            {
                System.out.println(card.toString());
            }
        }
    }

    public String GetNameOfTypeBranch(TypeBranch type)
    {
        if (type == TypeBranch.THUNGPHASANH)
            return "Thùng Phá Sảnh";
        if (type == TypeBranch.TUQUY)
            return "Tứ Quý";
        if (type == TypeBranch.CULU)
            return "Cù Lũ";
        if (type == TypeBranch.THUNG)
            return "Thùng";
        if (type == TypeBranch.SANH)
            return "Sảnh";
        if (type == TypeBranch.XAMCO)
            return "Sám Cô";
        if (type == TypeBranch.THU)
            return "Thú";
        if (type == TypeBranch.DOI)
            return "Đôi";
        if (type == TypeBranch.MAUTHAU)
            return "Mậu Thầu";
        return "";
    }

    public String GetNameOfTypeBranch(TypeBranch type,int index,int number_1)
    {
        if (type == TypeBranch.THUNGPHASANH && index==1)
            return "Thùng Phá Sảnh Chi 2";
        if (type == TypeBranch.TUQUY && number_1==1)
            return "Tứ Quý Át";
        if (type == TypeBranch.XAMCO && number_1==1 && index==2)
            return "Sám Cô Át";
        if (type == TypeBranch.TUQUY && index==1)
            return "Tứ Quý Chi 2";
        if (type == TypeBranch.CULU && index==1)
            return "Cù Lũ Chi 2";
        if (type == TypeBranch.XAMCO && index==2 )
            return "Sám Cô Chi 3";
        return GetNameOfTypeBranch (type);
    }
    public boolean CheckBranchSpecial(TypeBranch type,int index,int number_1)
    {
        if (type == TypeBranch.THUNGPHASANH)
            return true;
        if (type == TypeBranch.TUQUY)
            return true;
        if (type == TypeBranch.XAMCO && number_1==1 && index==2)
            return true;
        if (type == TypeBranch.CULU && index==1)
            return true;
        if (type == TypeBranch.XAMCO && index==2 )
            return true;
        return false;
    }

    public String GetNameOfTypeWinAbsolute(TypeWinAbsolute type)
    {
        if (type == TypeWinAbsolute.RONGLOC)
            return "RỒNG LỐC";
        if (type == TypeWinAbsolute.SANHRONG)
            return "SẢNH RỒNG";
        if (type == TypeWinAbsolute.NAMDOIMOTXAM)
            return "NĂM ĐÔI MỘT Sám";
        if (type == TypeWinAbsolute.LUCPHE)
            return "LỤC PHÉ";
        if (type == TypeWinAbsolute.BATHUNG)
            return "BA CÁI THÙNG";
        if (type == TypeWinAbsolute.BASANH)
            return "BA CÁI SẢNH";
        if (type == TypeWinAbsolute.CUNGMAU12)
            return "ĐỒNG HOA 12";
        if (type == TypeWinAbsolute.CUNGMAU13)
            return "ĐỒNG HOA 13";
        return "";
    }

    PlayerModel GetHost()
    {
        for (PlayerModel player : listPlayer)
            if (player.isHost)
                return player;
        return null;
    }

    Array<CardModel> listHost;
    public void SetDestroy(){
        isDestroy=true;
    }
    void Insert(Array<CardModel> array_original,Array<CardModel> array2,int x){
        int i=0;
        int size_need = array_original.size+array2.size;
        while (array_original.size<size_need)
        {
            array_original.insert(x+i,array2.get(i));
            i++;
        }
    }
}
