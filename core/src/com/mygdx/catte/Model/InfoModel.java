package com.mygdx.catte.Model;

import com.badlogic.gdx.graphics.Texture;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/14/2018.
 */

public class InfoModel {
    public int index;
    public String name;
    public long coin;
    public Texture avatar;
    public InfoModel(int index){
        this.index=index;
        Clear();
    }
    public void Clear(){
        if (index==0)
        {
            this.coin= Long.parseLong(PreferenceController.Instance.preferences.getString("HighScore"));
            this.name=PreferenceController.Instance.preferences.getString("PlayerName");
            if (UIController.Instance.startUI.textureAvatar!=null)
                this.avatar=UIController.Instance.startUI.textureAvatar;
            else
                this.avatar= Assets.GetTexture(Assets.GetAvatar(PreferenceController.Instance.preferences.getInteger("PlayerAvatar")));
        }
        else
        {
            this.coin= Long.parseLong(PreferenceController.Instance.preferences.getString("ScoreOfPlayer"+index));
            this.name = Utilities.GetNameOfPlayer(index);
            this.avatar= Assets.GetTexture(Assets.GetAvatar(index));
            //doi avatar
            if (this.index==PreferenceController.Instance.preferences.getInteger("PlayerAvatar"))
                this.avatar= Assets.GetTexture(Assets.GetAvatar(0));
        }
    }
}