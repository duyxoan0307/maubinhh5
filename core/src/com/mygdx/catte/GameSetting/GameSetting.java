package com.mygdx.catte.GameSetting;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/13/2018.
 */

public class GameSetting {
    public static GameSetting Instance;

    public Color colorShadow[];
    public Texture textureTypeCard;
    public boolean isSound;
    public Color[] arrColorTypeBranch;
    public boolean isAce;

    public GameSetting(){
        Instance=this;
        colorShadow=new Color[3];
        colorShadow[0]=Color.WHITE;
        colorShadow[1]= GameUI.NewColor(143,143,143,255);
        colorShadow[2]=GameUI.NewColor(56,56,56,255);

        arrColorTypeBranch=new Color[9];
        arrColorTypeBranch[0]= GameUI.NewColor("FFED00");
        arrColorTypeBranch[1]= GameUI.NewColor("FF1E1E");
        arrColorTypeBranch[2]= GameUI.NewColor("FF6F2F");
        arrColorTypeBranch[3]= GameUI.NewColor("409AFF");
        arrColorTypeBranch[4]= GameUI.NewColor("4BE8FF");
        arrColorTypeBranch[5]= GameUI.NewColor("4BE8FF");
        arrColorTypeBranch[6]= GameUI.NewColor("4FFF4B");
        arrColorTypeBranch[7]= GameUI.NewColor("F169FF");
        arrColorTypeBranch[8]= GameUI.NewColor("909090");
    }
}
