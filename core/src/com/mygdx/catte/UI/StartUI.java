package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.IZen;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/14/2018.
 */

public class StartUI extends Group {
    Button btnBatDau;
    Button btnLeaderBoard;
    Image imgBoxAvatar;
    Image imgBoxName;
    Label txtName;
    Image imgAvatarValue;
    Label txtCoin;
    Group groupInfo;
    Image imgBackground;
    Button btnGuide;
    Button btnSetting;
    Image logo_small;
    public StartUI(){
        CreateUI();
        UpdateInfoView();
        //CreateEffects();
        locateActors();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        //updateParticle(batch);
    }

    Array<ParticleEffect> arrParticleEffect;
    void CreateEffects() {
        arrParticleEffect=new Array<ParticleEffect>();

        ParticleEffect peStar = new ParticleEffect();
        peStar.load(Gdx.files.internal("Particle/star.p"),Gdx.files.internal("Particle/"));
        arrParticleEffect.add(peStar);
        peStar.setPosition(-MauBinh.V_WIDTH/2 + 250,MauBinh.V_HEIGHT/2 - 100);

        for (int i=0;i<1;i++)
        {
            final ParticleEffect peButtonStar = new ParticleEffect();
            peButtonStar.load(Gdx.files.internal("Particle/buttonSlide01.p"),Gdx.files.internal("Particle/"));
            peButtonStar.scaleEffect(2.3f);
            if (i==0)
            {
                peButtonStar.setPosition(btnBatDau.getX(Align.center)-100,btnBatDau.getY(Align.center)-20);
            }
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    arrParticleEffect.add(peButtonStar);
                }
            },i*2.5f);
        }

    }

    void CreateUI(){
        //imgBackground
        imgBackground = new Image(Assets.GetTexture(Assets.backgroundStart));
        imgBackground.setSize(MauBinh.V_WIDTH, MauBinh.V_HEIGHT);
        GameUI.SetPositionActor(imgBackground,0,0);


        //btnBatDau
        btnBatDau = GameUI.NewButton(Assets.GetTexture(Assets.btnBatDauMenu));
        btnBatDau.setPosition(-MauBinh.V_WIDTH/2+250,-60,Align.center);

        //btnLeaderBoard
        btnLeaderBoard = GameUI.NewButton(Assets.GetTexture(Assets.btnTopDaiGia));
        btnLeaderBoard.setPosition(-MauBinh.V_WIDTH/2+230,-240,Align.center);
        //logo
        //btnGuide
        btnGuide = GameUI.NewButton(Assets.GetTexture(Assets.btnGuide));
        btnGuide.setPosition(-663,63,Align.center);

        //btnSetting
        btnSetting = GameUI.NewButton(Assets.GetTexture(Assets.btnCaiDat));
        btnSetting.setPosition(663,63,Align.center);

        groupInfo=new Group();
        groupInfo.setPosition(0,0,Align.center);

        //info
        imgBoxAvatar=new Image(Assets.GetTexture(Assets.btnAvatarMenu));
        //imgBoxAvatar.setSize(125,125);
        imgBoxAvatar.setPosition(0,0,Align.topLeft);
        imgAvatarValue=new Image(Assets.GetTexture(Assets.GetAvatar(0)));
        imgAvatarValue.setSize(92,92);
        imgAvatarValue.setPosition(imgBoxAvatar.getX(Align.center),imgBoxAvatar.getY(Align.center),Align.center);
        imgAvatarValue.setOrigin(Align.center);

        imgBoxName=new Image(Assets.GetTexture(Assets.boxNameMenu));
        imgBoxName.setPosition(120,-80,Align.left);

        txtName=GameUI.NewLabel("Tran Khanh Duy",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        // GameUI.ScaleLabel(txtName,1f);
        txtName.setAlignment(Align.left);
        txtName.setPosition(imgBoxName.getX(Align.left),imgBoxName.getY(Align.center)+55,Align.left);

        txtCoin=GameUI.NewLabel("tran khanh duy",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtCoin.setWidth(imgBoxName.getWidth()-20);
        txtCoin.setAlignment(Align.left);
        txtCoin.setPosition(imgBoxName.getX(Align.center),imgBoxName.getY(Align.center)+2,Align.center);

        groupInfo.addActor(imgBoxAvatar);
        groupInfo.addActor(imgAvatarValue);

        groupInfo.addActor(imgBoxName);
        groupInfo.addActor(txtName);
        groupInfo.addActor(txtCoin);

        logo_small = new Image(Assets.GetTexture(Assets.logo));
        logo_small.setPosition(-MauBinh.V_WIDTH/2 + 100,MauBinh.V_HEIGHT/2 - 300);

        //addActor
        addActor(imgBackground);
        addActor(btnBatDau);
        addActor(btnLeaderBoard);
        addActor(btnSetting);
        addActor(btnGuide);
        addActor(groupInfo);
        addActor(logo_small);

        //addListener
        btnBatDau.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                UIController.Instance.onClickStartMenu();
            }
        });

        btnLeaderBoard.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                OverlayUI.Show();
                GameUI.Show(UIController.Instance.fbLeaderboardUI);
                UIController.Instance.fbLeaderboardUI.ShowLeaderboard();
            }
        });




        btnGuide.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();
                OverlayUI.Show();
                GameUI.Show(UIController.Instance.guideUI);

            }
        });
        btnSetting.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {

                SoundController.Instance.PlayClipButton();
                OverlayUI.Show();
                GameUI.Show(UIController.Instance.settingUI);

            }
        });

        btnBatDau.setOrigin(Align.center);
        Action scaleLoop = Actions.forever(Actions.sequence(Actions.scaleTo(1.05f,1.05f,0.5f), Actions.scaleTo(0.9f,0.9f,0.5f)));
        //btnBatDau.addAction(scaleLoop);
        SyncPlayerInfo();
    }

    public Texture textureAvatar;
    public void SyncPlayerInfo(){
        MauBinh.zenSDK.FBInstant_GetPlayerInfo(new IZen.FBInstant_PlayerInfoCallback() {
            @Override
            public void OnInfo(String name, String photoUrl) {
                PreferenceController.Instance.preferences.putString("PlayerName", name);

                if(photoUrl!="") {
                    MauBinh.zenSDK.LoadUrlTexture(photoUrl, new IZen.UrlTextureCallback() {
                        @Override
                        public void success(Texture texture) {
                            textureAvatar=texture;
                            GameUI.SetTextureForImage(imgAvatarValue, textureAvatar);
                        }

                        @Override
                        public void error() {

                        }
                    });
                }
            }
        });
    }

    public void SetCoin(){
        GameUI.SetTextAndFit(txtCoin, Utilities.ShortCutMoney(Long.parseLong(PreferenceController.Instance.preferences.getString("HighScore"))),1f);
    }

    public void UpdateInfoView(){
        Preferences preferences=PreferenceController.Instance.preferences;
        txtName.setText(preferences.getString("PlayerName"));
        // GameUI.SetTextureForImage(imgAvatarValue, Assets.GetTexture(Assets.GetAvatar(preferences.getInteger("PlayerAvatar"))));
        SetCoin();
    }
    public void Show(){
        //LeaderBoardUI.Instance.UpdateLeaderBoard();
        GameUI.ShowFade(this);
    }
    public void locateActors(){
        groupInfo.setPosition(-MauBinh.V_WIDTH/2 + 35,MauBinh.V_HEIGHT/2 - 15);

        imgBackground.setSize(MauBinh.V_WIDTH,MauBinh.V_HEIGHT);
        imgBackground.setPosition(0,0,Align.center);

        //btnGuide
        btnGuide.setPosition(MauBinh.V_WIDTH/2-100,MauBinh.V_HEIGHT/2-30,Align.topRight);

        //btnSetting
        btnSetting.setPosition(MauBinh.V_WIDTH/2-30,MauBinh.V_HEIGHT/2-30,Align.topRight);
        //btnBatDau
        btnBatDau.setPosition(-200,-MauBinh.V_HEIGHT/2+130,Align.center);

        //btnLeaderBoard
        btnLeaderBoard.setPosition(-btnBatDau.getX(Align.center),btnBatDau.getY(Align.center),Align.center);

        logo_small.setPosition(0,btnBatDau.getY(Align.top),Align.bottom);
    }

}
