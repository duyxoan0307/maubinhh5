package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.ParticleActor.ParticleEffectActor;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayingUI extends Group{
    public Button btnVanMoi;

    Button btnMenu;
    Image imgBoxTienCuoc;
    public Label txtMucCuoc;
    public Label txtTienCuoc;
    public Image imgCountScore;
    public Button btnXemLai;

    public ParticleEffectActor effectActor;
    Group groupTienCuoc;

    public PlayingUI(){
        CreateUI();
        AddUI();
        AddListener();
        CreateEffect();
        locateActors();
    }

    void CreateEffect() {
        final ParticleEffect pe = new ParticleEffect();
        pe.load(Gdx.files.internal("Particle/victory/victory.p"),Gdx.files.internal("Particle/victory"));
        effectActor=new ParticleEffectActor(pe);
        effectActor.setY(40);
        pe.scaleEffect(3f);
    }
    public void ShowEffect(){
        addActor(effectActor);
        effectActor.getEffect().reset(false);
    }
    public void HideEffect(){
        removeActor(effectActor);
    }
    void CreateUI(){
        btnVanMoi=GameUI.NewButton(Assets.GetTexture(Assets.btnVanMoi));
        btnVanMoi.setScale(1);
        GameUI.SetPositionActor(btnVanMoi,0,0);

        btnMenu=GameUI.NewButton(Assets.GetTexture(Assets.btnMenu));
        btnMenu.setPosition(845,440,Align.center);

        groupTienCuoc=new Group();
        groupTienCuoc.setPosition(0,0,Align.center);
        //imgBoxScoreBet
        imgBoxTienCuoc = new Image(Assets.GetTexture(Assets.boxTienCuoc));
        imgBoxTienCuoc.setPosition(0,0,Align.topLeft);

        //txtMucCuoc
        txtMucCuoc=GameUI.NewLabel("MỨC CƯỢC: ",Assets.GetBitmapFont(Assets.Saira_Regular));
        GameUI.ScaleLabel(txtMucCuoc,1.5f);
        txtMucCuoc.setAlignment(Align.left);
        txtMucCuoc.setPosition(imgBoxTienCuoc.getX(Align.left)+40,imgBoxTienCuoc.getY(Align.center),Align.left);

        //txtTienCuoc
        txtTienCuoc=GameUI.NewLabel("Tien cuoc: ",Assets.GetBitmapFont(Assets.Saira_Regular));
        GameUI.ScaleLabel(txtTienCuoc,1.5f);
        txtTienCuoc.setColor(Color.GREEN);
        txtTienCuoc.setAlignment(Align.left);
        txtTienCuoc.setWidth(170);
        txtTienCuoc.setPosition(txtMucCuoc.getX(Align.right)+80,txtMucCuoc.getY(Align.center),Align.left);

        groupTienCuoc.addActor(imgBoxTienCuoc);
        groupTienCuoc.addActor(txtMucCuoc);
        groupTienCuoc.addActor(txtTienCuoc);

        //test
        //Test();
        imgCountScore=new Image(Assets.GetTexture(Assets.img_chi1));
        imgCountScore.setVisible(false);

        btnXemLai=GameUI.NewButton(Assets.GetTexture(Assets.btnXemLai));
    }
    void AddUI(){
        addActor(btnVanMoi);
        addActor(btnMenu);
        addActor(groupTienCuoc);
        addActor(imgCountScore);
        addActor(btnXemLai);
    }
    public void SetCountScore(Texture texture){
        Vector2 cur_pos  = new Vector2(imgCountScore.getX(Align.right),imgCountScore.getY(Align.center));
        GameUI.SetTextureForImage(imgCountScore,texture);
        imgCountScore.setSize(texture.getWidth(),texture.getHeight());
        imgCountScore.setPosition(cur_pos.x,cur_pos.y,Align.right);
        imgCountScore.setVisible(true);
        imgCountScore.setOrigin(Align.center);
        imgCountScore.addAction(Actions.sequence(Actions.scaleTo(0,0),Actions.scaleTo(1,1,0.5f, Interpolation.swingOut)));
    }
    public void SetCountScore(boolean is){
        imgCountScore.setVisible(false);
    }
    void AddListener()
    {
        btnVanMoi.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                GameController.Instance.NewGame(false);
            }
        });

        btnMenu.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickPause();
            }
        });

        btnXemLai.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                btnXemLai.setVisible(false);
                btnVanMoi.setVisible(false);
                GameModel.Instance.CountScore();
            }
        });
    }
    public void onClickXepBai(){
        if (GameModel.Instance.GetMainPlayer().isCompletedSort)
            return;
        UIController.Instance.onClickXep();
    }
    public void SetScoreBet(){
        int score_bet = GameController.GetScoreBet();
        GameUI.SetTextAndFit(txtTienCuoc,Utilities.ShortCutMoney(score_bet),1.5f);
    }
    public void onClickPause(){
        OverlayUI.Show();
        GameUI.Show(UIController.Instance.pauseUI);
    }
    public void SetTextSoBai(){
        int so_bai=GameController.Instance.gameModel.listCardOnTable.size;
        String s="";
        if (so_bai>0)
            s+=""+so_bai;
    }
    public void locateActors(){
        groupTienCuoc.setPosition(-MauBinh.V_WIDTH/2+16,MauBinh.V_HEIGHT/2-16);
        btnMenu.setPosition(MauBinh.V_WIDTH/2-20,MauBinh.V_HEIGHT/2-20,Align.topRight);
        imgCountScore.setPosition(MauBinh.V_WIDTH/2-30,-MauBinh.V_HEIGHT/2+70,Align.right);

        btnXemLai.setPosition(btnMenu.getX(Align.left)-100,btnMenu.getY(Align.center),Align.right);
    }
}
