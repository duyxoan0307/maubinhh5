package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Model.BoardModel;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.PlayerModel;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Screen.GameScreen;
import com.mygdx.catte.Utilities.GameUI;


/**
 * Created by DUY on 8/27/2018.
 */

public class SortCardsUI extends Group {
    Image[] cardViews;
    Label[] txtLoaiChis;
    Image[] imgChecks;
    PlayerModel mainPlayer;
    Image overlay;
    Group groupCards;
    Group groupCheck;
    Group groupUI;

    public Button btnMauBinh;
    public Button btnXepXong;

    public SortCardsUI() {
        overlay = new Image(Assets.GetTexture(Assets.overlay));
        overlay.setSize(MauBinh.V_WIDTH, MauBinh.V_HEIGHT);
        overlay.setPosition(0, 0, Align.center);

        groupCards=new Group();
        groupCheck=new Group();
        groupCheck.setTouchable(Touchable.disabled);
        groupUI=new Group();
        addActor(overlay);
        addActor(groupCards);
        addActor(groupCheck);
        addActor(groupUI);

        mainPlayer = GameController.Instance.gameModel.GetMainPlayer();

        cardViews = new Image[13];
        for (int i = 0; i < 13; i++) {
            Image cardView = new Image(Assets.GetTexture(Assets.GetCard(0)));
            cardViews[i] = cardView;
            groupCards.addActor(cardView);
        }
        SetPositionCardViewFirst();

        for (int i=0;i<3;i++){
            Image over_loai_chi = new Image(Assets.GetTexture(Assets.overlay_black));
            over_loai_chi.setTouchable(Touchable.disabled);
            groupCheck.addActor(over_loai_chi);
            SetPositionOverlayLoaiChi(over_loai_chi,i);
        }
        txtLoaiChis=new Label[3];
        for (int i=0;i<3;i++)
        {
            Label txtLoaiChi = GameUI.NewLabel("Mậu thầu",Assets.GetBitmapFont(Assets.font_yellow));
            txtLoaiChi.setColor(Color.YELLOW);
            GameUI.ScaleLabel(txtLoaiChi,0.5f);

            txtLoaiChis[i]=txtLoaiChi;
            txtLoaiChi.setAlignment(Align.left);
            //txtLoaiChi.setDebug(true);
            groupCheck.addActor(txtLoaiChi);
            SetPositionLoaiChi(txtLoaiChi,i);
        }

        imgChecks=new Image[3];
        for (int i=0;i<3;i++)
        {
            Image imgCheck = new Image(Assets.GetTexture(Assets.imgTrue));
            imgChecks[i]=imgCheck;
            groupCheck.addActor(imgCheck);
        }
        SetPositionImageCheck();

        AddListener();

        //UI
        Button btnSwap = GameUI.NewButton(Assets.GetTexture(Assets.btnSwap));
        btnSwap.setPosition(-MauBinh.V_WIDTH/2+50,cardViews[5].getY(Align.bottom)-10,Align.left);

        btnXepXong = GameUI.NewButton(Assets.GetTexture(Assets.btnXepXong));

        btnXepXong.setPosition(MauBinh.V_WIDTH/2-30,-MauBinh.V_HEIGHT/2+30,Align.bottomRight);
        btnXepXong.addListener(new IClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                OnEndSort();
            }
        });
        btnSwap.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                OnSwapChi();
            }
        });

        btnMauBinh = GameUI.NewButton(Assets.GetTexture(Assets.btnMauBinh));

        btnMauBinh.setPosition(btnXepXong.getX(Align.center),btnXepXong.getY(Align.top)+50,Align.bottom);
        btnMauBinh.addListener(new IClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                GameController.Instance.gameModel.listPlayer.get(0).SetOnMauBinh ();
                OnEndSort();
            }
        });
        btnMauBinh.setOrigin(Align.center);
        Action scaleLoop = Actions.forever(Actions.sequence(Actions.scaleTo(1.05f,1.05f,0.5f), Actions.scaleTo(0.9f,0.9f,0.5f)));
        btnMauBinh.addAction(scaleLoop);

        groupUI.addActor(btnSwap);
        groupUI.addActor(btnXepXong);
        groupUI.addActor(btnMauBinh);
    }

    public void OnEndSort(){
        Hide();
        GameController.Instance.OnCompleteSort ();
        GameController.Instance.DisableSort ();
    }

    public void RegisterEvent() {
        Reset();
        for (int i = 0; i < 13; i++) {
            final Image cardView = cardViews[i];
            CardModel cardModel = mainPlayer.GetListCards().get(i);
            GameUI.SetTextureForImage(cardView, Assets.GetTexture(Assets.GetCard(BoardModel.Instance.GetIndexOfCardInBoard(cardModel))));

            cardModel.RegisterSetShadow(new CardModel.OnSetShadow() {
                @Override
                public void OnEvent(int level) {
                    cardView.setColor(GameSetting.Instance.colorShadow[level]);
                }
            });
        }
        mainPlayer.RegisterSortList(new PlayerModel.OnSortList() {
            @Override
            public void OnEvent(int index_list) {
                CheckChi();
            }
        });
        mainPlayer.RegisterSetTypeBranch(new PlayerModel.OnSetTypeBranch() {
            @Override
            public void OnEvent(int index_list,GameModel.TypeBranch type_branch,boolean isSpecial,int number_1) {
                String s="";
                if (!isSpecial)
                    s=GameModel.Instance.GetNameOfTypeBranch(type_branch);
                else
                    s=GameModel.Instance.GetNameOfTypeBranch(type_branch,index_list,number_1);
                txtLoaiChis[index_list].setText(s);
                //  txtLoaiChis[index_list].setColor(GameSetting.Instance.arrColorTypeBranch[type_branch.ordinal()]);
            }
        });
        mainPlayer.RegisterBranchIsValid(new PlayerModel.OncbBranchIsValid() {
            @Override
            public void OnEvent(final int index, final boolean is_valid) {

                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        final Texture texture;
                        if (is_valid)
                            texture=Assets.GetTexture(Assets.imgTrue);
                        else
                            texture=Assets.GetTexture(Assets.imgFalse);
                        GameUI.SetTextureForImage(imgChecks[index],texture);
                    }
                });
            }
        });

        mainPlayer.SetUpdateListHold();

        SetPositionCardView();
    }

    public void Show() {
        RegisterEvent();
        GameUI.ShowFade(this);
    }

    public void Hide() {
        GameUI.HideFade(this);
    }

    void SetPositionCardView() {
        for (int i = 0; i < 13; i++) {
            Image cardView = cardViews[i];
            Vector2 position = IndexToPosition(i);
            cardView.addAction(Actions.moveToAligned(position.x,position.y,Align.center,0.25f));
        }
    }
    void SetPositionCardViewFirst() {
        for (int i = 0; i < 13; i++) {
            Image cardView = cardViews[i];
            Vector2 position = IndexToPosition(i);
            cardView.setPosition(position.x,position.y,Align.center);
        }
    }
    void SetPositionLoaiChi(Label txt,int i){
        Image tmp;
        switch (i)
        {
            case 0:
                tmp = cardViews[0];
                break;
            case 1:
                tmp = cardViews[5];
                break;
            case 2:
                tmp = cardViews[10];
                break;
            default:
                tmp=cardViews[0];
                break;
        }
        txt.setPosition(tmp.getX()+40,tmp.getY()-22);
    }
    void SetPositionOverlayLoaiChi(Image img,int i){
        Image tmp;
        switch (i)
        {
            case 0:
                tmp = cardViews[0];
                break;
            case 1:
                tmp = cardViews[5];
                break;
            case 2:
                tmp = cardViews[10];
                break;
            default:
                tmp=cardViews[0];
                break;
        }
        img.setPosition(tmp.getX(),tmp.getY());
    }
    void SetPositionImageCheck(){
        Vector2 pos_0=IndexToPosition(0);
        imgChecks[0].setPosition(pos_0.x-cardViews[0].getWidth()/2-50,pos_0.y,Align.right);
        pos_0=IndexToPosition(5);
        imgChecks[1].setPosition(pos_0.x-cardViews[0].getWidth()/2-50,pos_0.y,Align.right);
        pos_0=IndexToPosition(10);
        imgChecks[2].setPosition(pos_0.x-cardViews[0].getWidth()/2-50,pos_0.y,Align.right);
    }
    Vector2 IndexToPosition(int index) {
        float distance_X = cardViews[0].getWidth()+45;
        float distance_Y = cardViews[0].getHeight()+45;
        float pos_Y = 0;
        if (index <= 4)
            pos_Y = -distance_Y;
        else if (index <= 9)
            pos_Y = 0;
        else
            pos_Y = distance_Y;

        int reIndex = (index % 5 - 2);
        //System.out.println(reIndex);
        return new Vector2(reIndex * distance_X, pos_Y);
    }

    int PositionToIndex() {
        return 0;
    }

    void AddListener() {
        for (int i = 0; i < 13; i++) {
            final Image cardView = cardViews[i];
            cardView.addListener(new IClickListener() {
                public void touchDragged(InputEvent event, float x, float y, int pointer) {
                    Vector3 position = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
                    position = getStage().getCamera().unproject(position);
                    cardView.setPosition(position.x, position.y, Align.center);
                    super.touchDragged(event, x, y, pointer);
                }

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    cardView.toFront();
                    return super.touchDown(event, x, y, pointer, button);
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                    Image card_swap = FindNearest(cardView);
                    if (card_swap!=null)
                    {
                        int index_1=GetIndex(card_swap);
                        int index_2=GetIndex(cardView);
                        SwapTwoInList(index_1,index_2);
                        mainPlayer.SwapTwoCards(index_1,index_2);
                    }
                    SetPositionCardView();
                }
            });
        }
    }

    Image FindNearest(Image my_card) {
        Array<Image> arr = FindColliders(my_card);
        if (arr.size == 0)
            return null;
        if (arr.size == 1)
            return arr.get(0);
        Image card_result = arr.get(0);
        for (int i = 1; i < arr.size; i++) {
            Image cardView = arr.get(i);
            if (Distance(cardView, my_card) < Distance(card_result, my_card))
                card_result = cardView;
        }
        return card_result;
    }

    Array<Image> FindColliders(Image my_card) {
        Array<Image> arr = new Array<Image>();
        Rectangle my_card_bound = GetBound(my_card);
        for (int i = 0; i < 13; i++) {
            Image cardView = cardViews[i];
            Rectangle bounds = GetBound(cardView);
            if (my_card == cardView)
                continue;
            if (my_card_bound.overlaps(bounds)) {
                arr.addAll(cardView);
            }
        }
        return arr;
    }

    Rectangle GetBound(Image image) {
        return new Rectangle(image.getX(), image.getY(), image.getWidth(), image.getHeight());
    }

    float Distance(Image card_1, Image card_2) {
        return Vector2.dst(card_1.getX(), card_1.getY(), card_2.getX(), card_2.getY());
    }
    void SwapTwoInList(int x,int y)
    {
        Image tmp=cardViews[x];
        cardViews[x]=cardViews[y];
        cardViews[y]=tmp;

    }
    int GetIndex(Image card){
        for (int i=0;i<cardViews.length;i++)
        {
            if (cardViews[i]==card)
                return i;
        }
        return -1;
    }
    void OnSwapChi(){
        int index_1;
        int index_2;
        for (int i=0;i<5;i++)
        {
            index_1=i;
            index_2=i+5;
            SwapTwoInList(index_1,index_2);
            mainPlayer.SwapTwoCards(index_1,index_2);
        }
        SetPositionCardView();
    }
    void CheckChi(){
        //check binh lung
    }
    public void locateActors(){
        btnXepXong.setPosition(MauBinh.V_WIDTH/2-30,-MauBinh.V_HEIGHT/2+30,Align.bottomRight);
        btnMauBinh.setPosition(btnXepXong.getX(Align.center),btnXepXong.getY(Align.top)+50,Align.bottom);
        overlay.setSize(MauBinh.V_WIDTH,MauBinh.V_HEIGHT);
        overlay.setPosition(0,0,Align.center);
    }
    void Reset(){
        for (int i=0;i<13;i++)
            cardViews[i].setColor(GameSetting.Instance.colorShadow[0]);
    }
}
