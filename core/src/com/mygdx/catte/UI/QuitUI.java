package com.mygdx.catte.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/15/2018.
 */

public class QuitUI extends Group{
    public QuitUI(){
        CreateUI();
    }
    void CreateUI(){
        //imgBox
        Image imgBox = new Image(Assets.GetTexture(Assets.imgBox));
        imgBox.setPosition(0,0,Align.center);

        //btnExit
        Button btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        GameUI.SetPositionActor(btnExit, imgBox.getX() + imgBox.getWidth() - 20, imgBox.getY() + imgBox.getHeight() - 20);

        //txtTitle
        Label txtTitle = GameUI.NewLabel("THOÁT GAME", Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle, 2);
        txtTitle.setPosition(0,imgBox.getY(Align.top)-60,Align.center);

        //txtContent
        Label txtContent = GameUI.NewLabel("Bạn có muốn thoát game không?",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtContent, 2);
        txtContent.setWidth(imgBox.getWidth()-30);
        txtContent.setWrap(true);
        txtContent.setAlignment(Align.top);
        txtContent.setPosition(0,imgBox.getY(Align.top)-150,Align.top);

        //btnYes
        Button btnYes = GameUI.NewButton(Assets.GetTexture(Assets.btnThoat));
        GameUI.SetPositionActor(btnYes,-260,-160);

        //btnNo
        Button btnNo = GameUI.NewButton(Assets.GetTexture(Assets.btnTiepTuc));
        GameUI.SetPositionActor(btnNo,260,-160);

        //add actor
        addActor(imgBox);
        addActor(btnExit);
        addActor(txtTitle);
        addActor(txtContent);
        addActor(btnYes);
        addActor(btnNo);

        //addListener
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickExitQuit();
            }
        });
        btnNo.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickExitQuit();
            }
        });
        btnYes.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                Gdx.app.exit();
            }
        });
    }
    public void onClickExitQuit(){
        OverlayUI.Hide();
        GameUI.Hide(QuitUI.this);
    }
}

