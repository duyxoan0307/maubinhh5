package com.mygdx.catte.UI;



import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.SoundController;
import com.mygdx.catte.Controller.UIController;
import com.mygdx.catte.Others.IClickListener;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/15/2018.
 */

public class GuideUI extends Group {
    Image imgBox;
    public GuideUI(){
        CreateUI();
        GameUI.HideNonAction(this);
    }

    private void CreateUI() {
        //imgBox
        imgBox = new Image(Assets.GetTexture(Assets.imgBox));
        //  imgBox.setSize(Catte.V_WIDTH-200,Catte.V_HEIGHT-300);
        GameUI.SetPositionActor(imgBox,0,0);

        //btnExit
        Button btnExit = GameUI.NewButton(Assets.GetTexture(Assets.btnExit));
        GameUI.SetPositionActor(btnExit,imgBox.getX()+imgBox.getWidth()-20,imgBox.getY()+imgBox.getHeight()-20);

        //txtTitle
        Label txtTitle = GameUI.NewLabel("HƯỚNG DẪN CHƠI MẬU BINH",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(txtTitle,1.5f);
        GameUI.SetPositionActor(txtTitle,0,imgBox.getY(Align.top)-50);

        //add actor
        addActor(imgBox);
        addActor(btnExit);
        addActor(txtTitle);

        //create scroll pane
        CreateScrollPane();

        //addListener
        btnExit.addListener(new IClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                SoundController.Instance.PlayClipButton();
                onClickExitGuide();

            }
        });
    }

    private void CreateScrollPane() {
        ScrollPane scrollpane;
        Table container;

        // table that holds the scroll pane
        container = new Table();
        container.setWidth(imgBox.getWidth()-50);
        container.setHeight(imgBox.getHeight()-150);

        GameUI.SetPositionActor(container,0,-50);


        //textGuide
        String contentGuide="Giới thiệu\n" +
                "Trò chơi sử dụng bộ bài chuẩn 52 lá. Mỗi người sẽ được chia 13 lá bài, xếp thành 3 chi. Chi đầu và chi 2 gồm 5 lá bài, chi cuối gồm 3 lá bài. Người chơi phải sắp xếp sao cho chi trước mạnh hơn chi sau. Các chi có liên kết theo quy định. Sau khi hoàn thành, bài sẽ được so sánh theo từng chi. Người chơi thắng được nhiều chi nhất sẽ thắng cuộc.\n" +
                "Các lá bài\n" +
                "Một lá bài gồm hai phần, số và chất: lá 8♣ có số là 8 và chất là ♣ (tép).\n" +
                "Giá trị quân bài chỉ phụ thuộc vào số. Xếp hạng độ mạnh như sau:\n" +
                "A (át) > K (già) > Q (đầm) > J (bồi) > 10 > 9 > 8 > 7 >...> 3 > 2.\n" +
                "\n" +
                "Liên kết\n" +
                "Các lá bài của người chơi có liên kết được quy định như sau. Độ mạnh tăng dần từ trên xuống dưới.\n" +
                "- Mậu thầu: Không có liên kết các lá bài.\n" +
                "VD: A♠ Q♣ 10♥ 9♦ 8♥\n" +
                "- Đôi:  7♦, 7♣, 10♠, Q♣, A♠\n" +
                "- Thú:  2 đôi (chi cuối không có). VD: J♦ J♣ 9♠ 9♥ K♣\n" +
                "- Sám ( 3 lá cùng số):  K♥ K♦ K♣, A♣, 2♠\n" +
                "- Sảnh: 5 lá có số liên tiếp nhau (chi cuối không có). Liên kết A, 2, 3, 4, 5 cũng gọi là sảnh nhưng là sảnh thấp nhất.\n" +
                " VD sảnh:J♥ 10♦ 9♣ 8♠ 7♦\n" +
                "- Thùng: 5 lá cùng chất (chi cuối không có). VD: 7♦ Q♦ 10♦ K♦ A♦ \n" +
                "- Cù Lũ: 1 Sám & 1 Đôi (chi cuối không có). VD: Q♥ Q♦ Q♠ 9♣ 9♠\n" +
                "- Tứ Quý: 4 lá cùng số (chi cuối không có). VD: Q♥ Q♦ Q♣ Q♠, K♠\n" +
                "- Thùng phá Sảnh: Dây đồng chất (chi cuối không có). VD: Q♥ J♥ 10♥ 9♥ 8♥\n" +
                "- Thùng phá Sảnh lớn: Dây đồng chất có A ( chi cuối không có). VD: A♠ K♠ Q♠ J♠ 10♠ \n" +
                "Lưu ý: Nếu khi so 2 chi bằng nhau thì người cầm cái sẽ được ưu tiên thắng chi đó\n" +
                "\n" +
                "Cách chơi\n" +
                "  - Bắt đầu ván chơi, mỗi người được chia 13 lá, xếp thành 3 chi. Người chơi có 60s để sắp xếp bài của mình sao cho liên kết của chi trước mạnh hơn chi sau.\n" +
                "  - Người chơi có thể chọn gợi ý để máy tự sắp bài.\n" +
                "  - Sau khi hết thời gian hoặc xếp xong bài thì bắt đầu đọ bài. Lần lượt từng người chơi sẽ so từng chi với nhau. Mỗi lần so sánh kéo dài 5s gồm hiển thị bộ bài, tên bộ bài, số lần thắng được cập nhật.\n" +
                "  - Nếu Người chơi có bài Mậu Binh thắng trắng thì sẽ thắng luôn mà không cần so bài.\n" +
                "  - Nếu độ mạnh bằng nhau thì cùng chia gà.\n" +
                "\n" +
                "Các trường hợp Mậu Binh thắng trắng: Với  Độ mạnh giảm dần như sau:\n" +
                "  - Rồng lốc: Gồm 13 lá thừ 2 đến A và đồng chất. Người chơi được 72 lần tiền cược.\n" +
                "  - Sảnh rồng: Gồm 13 lá bài từ 2 đến A không đồng chất. Người chơi được 36 lần tiền cược.\n" +
                "  - Năm đôi 1 sám: Gồm 5 đôi và 1 bộ ba trong 13 lá bài. Người chơi được 36 lần tiền cược.\n" +
                "  - Lục phé bôn: Gồm 6 đôi trong 13 lá bài. Người thắng được 18 lần tiền cược.\n" +
                "  - Ba thùng: Gồm 3 thùng tại cả 3 chi. Người chơi được 18 lần tiền cược.\n" +
                "  - Ba sảnh: Gồm 3 sảnh tại cả 3 chi. Người chơi dược 18 lần tiền cược.\n" +
                "  - Đồng hoa 13 lá: Gồm 13 lá cùng màu trong 13 là bá. Người chơi được 30 lần tiền cược. \n" +
                "  - Đồng hoa 12 lá: Gồm 12 lá cùng màu trong 13 là bá. Người chơi được 24 lần tiền cược.\n" +
                "Lưu ý: Trong trường hợp được Rồng lốc, Sảnh rồng, Đồng hoa, máy sẽ tự báo Ăn trắng và kết thúc xếp bài. Các trường hợp còn lại người chơi phải tự tìm ra cách xếp để ăn trắng, khi sắp xếp đúng máy sẽ hiện nút Mậu binh, nhấn vào nút đó để nhận Ăn trắng, nếu không sẽ tính 3 chi như bình thường. Điều này giúp người chơi linh hoạt trong cách sắp xếp tìm ra ăn trắng để áp dụng thực tế.\n" +
                "\n" +
                "Các trường hợp mậu binh thường\n" +
                "  - Sám chi cuối:  Nếu người chơi thắng chi cuối bằng bộ sám thì sẽ được 6 lần tiền cược. Trường hợp tính Át và xám cô này là xám cô Át thì sẽ được 20 lần tiền cược.\n" +
                "  - Cù lũ chi 2:  Nếu người chơi thắng chi 2 bằng bộ cù lũ sẽ được 4 lần cược.\n" +
                "  - Tứ quý chi đầu:  Nếu người chơi thắng chi đầu bằng bộ tứ quý thì sẽ được 8 lần cược.\n" +
                "  - Tứ quý chi 2:  Nếu người chơi thắng chi 2 bằng bộ tứ quý thì sẽ được 16 lần cược. Trường hợp tính Át và tứ quý này là tứ quý Át thì sẽ được 20 lần tiền cược.\n" +
                "  - Thùng phá sảnh chi đầu:  Người chơi thắng chi đầu bằng bộ thùng phá sảnh thì sẽ được 10 lần cược.\n" +
                "  - Thùng phá sảnh chi 2:  Người chơi thắng chi 2 bằng bộ thùng phá sảnh thì sẽ được 20 lần cược.\n" +
                "\n" +
                "Tính tiền\n" +
                "  + Người thắng:  Người thắng 1 chi sẽ được nhận 1 cược từ người thua.\n" +
                "  + Người thua:  Sẽ phải đền số chi bị thua tương đương với số cược cho người thắng.\n" +
                "  + Sập 3 chi:  Là người bị thua cả 3 chi, sẽ phải đền thêm 3 chi tiền cược cho người thắng.\n" +
                "  + Sập làng:  Là 1 người chơi thắng cả 3 chi với các nhà còn lại, mỗi nhà đền thêm 3 chi tiền cược cho người bắt sập làng.\n" +
                "+ Tính Át:  Người chơi sẽ được/mất (Tổng số người chơi x Số át mình đang giữ - Tổng số át trên bàn) x tiền cược\n";
        Label txtGuide = GameUI.NewLabel(contentGuide,Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtGuide.setAlignment(Align.left);
        txtGuide.setWrap(true);
        GameUI.ScaleLabel(txtGuide,1f);

        //inner table that is used as a makeshift list.
        Table innerContainer = new Table();
        innerContainer.add(txtGuide).expand().fill();

        // create the scrollpane
        scrollpane = new ScrollPane(innerContainer);

        scrollpane.setScrollingDisabled(true,false);

        //add the scroll pane to the containe
        container.add(scrollpane).fill().expand();
        addActor(container);
    }
    public void onClickExitGuide(){
        OverlayUI.Hide();
        GameUI.Hide(this);
    }
}
