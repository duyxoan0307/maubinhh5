package com.mygdx.catte.UI;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Utilities.GameUI;

public class MachineSortingUI extends Group {
    public MachineSortingUI(){
        Label txtDangXep = GameUI.NewLabel("Máy đang xếp bài...",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        txtDangXep.setPosition(0,0,Align.center);
        GameUI.ScaleLabel(txtDangXep,2f);
        txtDangXep.setAlignment(Align.center);
        addActor(txtDangXep);
    }
    public void Show(){
        OverlayUI.Show();
        GameUI.ShowFade(this);
    }
    public void Hide(){
        OverlayUI.Hide();
        GameUI.Hide(this);
    }
}
