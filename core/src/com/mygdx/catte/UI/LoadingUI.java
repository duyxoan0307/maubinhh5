package com.mygdx.catte.UI;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 8/9/2018.
 */

public class LoadingUI extends Group{
    Image imgBackground;
    Image imgLogo;
    Image loadingCircle;
    Texture backgroundTexture;
    Texture logoTexture;
    Texture loadingCircleTexture;
    BitmapFont sairaFont;

    public LoadingUI(){
        backgroundTexture=GameUI.NewTexture("UI/backgroundStart.jpg");
        imgBackground = new Image(backgroundTexture);
        imgBackground.setSize(MauBinh.V_WIDTH,MauBinh.V_HEIGHT);

        logoTexture=GameUI.NewTexture("UI/logo.png");
        imgLogo=new Image(logoTexture);

        loadingCircleTexture=GameUI.NewTexture("UI/loading_circle.png");
        loadingCircle = new Image(loadingCircleTexture);
        loadingCircle.setOrigin(Align.center);

        sairaFont= GameUI.NewFont("Saira_ExtraBold");

        loadingCircle.setPosition(0,-MauBinh.V_HEIGHT/2+100,Align.bottom);
        Label txtLoading = GameUI.NewLabel("Loading...","Saira_ExtraBold");
        GameUI.ScaleLabel(txtLoading,2);
        txtLoading.setPosition(0,loadingCircle.getY(Align.bottom)-10,Align.top);

        addActor(imgBackground);
        addActor(imgLogo);
        addActor(txtLoading);
        addActor(loadingCircle);
        locateActors();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        loadingCircle.rotateBy(-1);
    }

    public void locateActors(){
        imgBackground.setSize(MauBinh.V_WIDTH,MauBinh.V_HEIGHT);
        imgBackground.setPosition(0,0, Align.center);
        imgLogo.setPosition(0,-MauBinh.V_HEIGHT/2+200,Align.bottom);
    }
    public void dispose(){
        backgroundTexture.dispose();
        logoTexture.dispose();
        loadingCircleTexture.dispose();
        sairaFont.dispose();
    }
}
