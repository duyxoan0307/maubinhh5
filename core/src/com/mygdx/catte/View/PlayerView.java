package com.mygdx.catte.View;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/12/2018.
 */

public class PlayerView extends Group {
    public ListCardView[] listChi;
    public String name;
    public int index;
    public InfoView infoView;
    public Image imgtReport;
    public PlayerView(int index){
        //set index
        this.index=index;
        this.name="Player"+index;

        //create list
        listChi=new ListCardView[3];
        float scale;
        scale=0.65f;
        for (int i=0;i<3;i++)
        {
            ListCardView list_phom = new ListCardView(scale,this);
            listChi[i]=list_phom;
        }

        imgtReport=new Image(Assets.GetTexture(Assets.img_binhlung));
        imgtReport.setVisible(false);
        //create info view
        CreateInfoView();
        //add actor
        AddActor();
    }

    public ListCardView GetListByIndex(int index){
        if (index<=listChi.length)
            return listChi[index];
        return null;
    }

    public void SetUnChildToSort(int index_list){
        GetListByIndex(index_list).Clear();
    }

    void CreateInfoView(){
        String name = "ToiLaAi";
        int coin = 1000000;
        int index_avatar=this.index;
        infoView=new InfoView(name,coin,index_avatar);
    }
    void AddActor(){
        this.addActor(infoView);
        for (int i=2;i>=0;i--)
            this.addActor(listChi[i]);
        this.addActor(imgtReport);
    }
    public void SetPositionInfoView(){
        switch (this.index){
            case 0:
                infoView.setPosition(-350,-MauBinh.V_HEIGHT/2+110);
                break;
            case 1:
                infoView.setPosition(MauBinh.V_WIDTH/2-110,0);
                break;
            case 2:
                infoView.setPosition(-220,MauBinh.V_HEIGHT/2-120);
                break;
            case 3:
                infoView.setPosition(-MauBinh.V_WIDTH/2+110,0);
                break;
        }
    }

    public void Clear(){
        for (ListCardView list:listChi)
            list.Clear();
        infoView.Clear();
        imgtReport.setVisible(false);
    }
    public void SetReport(Texture image){
        imgtReport.setVisible(true);
        Vector2 cur_pos = new Vector2(imgtReport.getX(Align.center),imgtReport.getY(Align.bottom));
        imgtReport.setSize(image.getWidth(),image.getHeight());
        GameUI.SetTextureForImage(imgtReport,image);
        imgtReport.setPosition(cur_pos.x,cur_pos.y,Align.bottom);
        imgtReport.setOrigin(Align.center);
        imgtReport.addAction(Actions.sequence(Actions.scaleTo(0,0),Actions.scaleTo(1,1,0.5f, Interpolation.swingOut)));
    }
    public void SetReport(boolean is_show){
        imgtReport.setVisible(false);
    }
    public void UpdateGroupListCardView(){
        for (int i=0;i<3;i++)
            listChi[i].UpdateGroup();
    }
}