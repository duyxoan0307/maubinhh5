package com.mygdx.catte.View;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.Controller.GameController;
import com.mygdx.catte.Utilities.GameUI;

/**
 * Created by DUY on 6/12/2018.
 */

public class ListCardView extends Group {
    public float width;
    public float height;
    public Array<CardView> listCardViews;
    public float scale;
    Group group;
    public PlayerView playerView;
    public Label typeName;
    public ListCardView(float scale,PlayerView playerView){
        this.playerView=playerView;
        this.scale=scale;
        CardView cardView=new CardView();
        cardView.SetImage(Assets.GetTexture(Assets.GetCard(0)));
        this.width= Assets.GetTexture(Assets.GetCard(0)).getWidth()*scale;
        this.height= Assets.GetTexture(Assets.GetCard(0)).getHeight()*scale;
        listCardViews=new Array<CardView>();
        //test
        //this.addActor(cardView);
        cardView.image.setSize(width,height);
        cardView.image.setPosition(getX()-width/2,getY()-height/2);
        //SortList();
        group=new Group();
        addActor(group);

        typeName= GameUI.NewLabel("Report",Assets.GetBitmapFont(Assets.Saira_ExtraBold));
        GameUI.ScaleLabel(typeName,0.5f);
        typeName.setWidth(0);
        typeName.setAlignment(Align.center);
    }
    public void AddCardView(CardView cardView){
        listCardViews.add(cardView);
        cardView.SetParent(this);
        group.addActor(cardView);
    }
    public void SortCenterDelay(float distance_X){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;
        for (final CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            final float offset = (current - center) * width * distance_X + is_not_odd* distance_X * width /2;
            cardView.SetPosition(getX()-width/2+offset,getY()-height/2-400);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            cardView.image.addAction(Actions.moveTo(getX()-width/2+offset,getY()-height/2,0.5f, Interpolation.swingOut));
                        }
                    },0.2f*listCardViews.indexOf(cardView,false));
                }
            },0.5f);
        }
    }
    public void SortCenter(float distance_X){
        int center = listCardViews.size/2;
        int is_not_odd = (listCardViews.size+1)%2;

        center=5/2;
        is_not_odd=(5+1)%2;
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = (current - center) * width * distance_X + is_not_odd* distance_X * width /2;

            float y;
            if (cardView.positionType== CardView.PositionType.UP && cardView.canClick)
            {
                y=cardView.image.getY();
            }
            else
                y=getY()-height/2;
            cardView.SetPosition(getX()-width/2+offset,y);
        }
    }
    public void SortLeft(float distance_X){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = current*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void SortRight(float distance_X){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);

            int current = listCardViews.indexOf(cardView,false);
            int sorting_order = current;
            cardView.setZIndex(sorting_order);
            float offset = (current-listCardViews.size+1)*width*distance_X;

            cardView.SetPosition(getX()-width/2+offset,getY()-height/2);
        }
    }
    public void MoveToParent(){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);
            cardView.SetPosition(getX()-width/2,getY()-height/2);
        }
    }
    public void MoveToParentCenter(ListCardView parent){
        for (CardView cardView:listCardViews
                ) {
            cardView.SetSize(width,height);
            cardView.SetPosition(parent.getX()-width/2,parent.getY()-height/2);
        }
    }
    public void SortZIndexListTrung(){
        for (CardView cardView:listCardViews
                ) {
            cardView.toFront();
        }
    }
    public void UpdateGroup(){
        Vector2 position = this.stageToLocalCoordinates(new Vector2(0,0));
        group.setPosition(position.x,position.y);
    }
    public void Clear(){
        group.clearChildren();
        listCardViews.clear();
        typeName.setVisible(false);
    }
}
