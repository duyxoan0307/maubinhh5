package com.mygdx.catte.Preferences;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.mygdx.catte.Model.DataModel;


/**
 * Created by DUY on 6/14/2018.
 */

public class PreferenceController {
    public static PreferenceController Instance;
    public Preferences preferences;
    public PreferenceController(){
        Instance=this;
        preferences= Gdx.app.getPreferences("MauBinh");
        InitPreference();
        CheckComputerCoin();
        preferences.flush();
    }

    private void InitPreference() {
//        preferences.clear();
//        preferences.flush();
        if (!preferences.contains("Host"))
        {
            ResetPlayerPreferences();
        }
    }
    void CheckComputerCoin(){
        for (int i=1;i<13;i++)
        {
            if (Long.parseLong(preferences.getString("ScoreOfPlayer"+i))>=500000)
                continue;;
            int ran = (int) (10 + Math.random()*(30-10));
            preferences.putString("ScoreOfPlayer"+i,String.valueOf((long)(ran*1000000)));
            preferences.flush();
        }
    }
    public void ResetPlayerPreferences(){
        preferences.clear();
        preferences.flush();
        DataModel.SReset();
        preferences.putInteger("Host",0);
        preferences.putString("HighScore",1000000+"");
        preferences.putInteger("ScoreBet",0);
        preferences.putInteger("NumberPlayer",4);
        preferences.putBoolean("IsSound",true);
        preferences.putInteger("TypeCard",0);
        preferences.putBoolean("IsAce",false);
        if (!preferences.contains("MaxScoreBet"))
            preferences.putInteger("MaxScoreBet",0);
        for (int i=1;i<13;i++)
        {
            int ran = (int) (10 + Math.random()*(30-10));
            preferences.putString("ScoreOfPlayer"+i,String.valueOf((long)(ran*1000000)));
            preferences.flush();
        }
    }
}
