package com.mygdx.catte.Controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.catte.AssetManager.Assets;
import com.mygdx.catte.MauBinh;
import com.mygdx.catte.Model.DataModel;
import com.mygdx.catte.GameSetting.GameSetting;
import com.mygdx.catte.Model.CardModel;
import com.mygdx.catte.Model.GameModel;
import com.mygdx.catte.Model.InfoModel;
import com.mygdx.catte.Model.PlayerModel;
import com.mygdx.catte.Preferences.PreferenceController;
import com.mygdx.catte.Screen.GameScreen;
import com.mygdx.catte.UI.OverlayUI;
import com.mygdx.catte.UI.PlayingUI;
import com.mygdx.catte.Utilities.GameUI;
import com.mygdx.catte.Utilities.Utilities;
import com.mygdx.catte.View.CardView;
import com.mygdx.catte.View.ListCardView;
import com.mygdx.catte.View.PlayerView;

import java.util.Arrays;
import java.util.Random;


/**
 * Created by DUY on 6/12/2018.
 */

public class GameController extends Group {
    public static GameController Instance;

    public PlayerView[] playerViews;
    PlayerView[] playerViewsTemp;

    Image background;
    Group deck;
    CardView[] arrCardViews;
    public GameModel gameModel;
    public Array<Integer> listPlayerInfo;
    Array<InfoModel> listInfoModel;

    Array<Actor> listQuanLyScore;
    Array<Actor> listQuanLyRank;
    Array<Actor> listQuanLyArrow;

    public static CardModel currentChooseCard;
    public static Array<CardModel> listChooseCard;
    public GameController() {
        Instance = this;
        new PreferenceController();
        //init setting
        new GameSetting();
        //addBackground
        gameModel=new GameModel(2,4,false);
        new DataModel();
        AddBackGround();
        playerViews = new PlayerView[4];
        //create group deck
        CreateDecks();
        //init list player views
        SetListPlayerViews();
        //listChooseCard
        listChooseCard=new Array<CardModel>();
        //init list quan ly
        listQuanLyScore=new Array<Actor>();
        listQuanLyRank=new Array<Actor>();
        listQuanLyArrow=new Array<Actor>();
        //Test

        locateActors();
    }

    public void AddToStage(boolean is_add){
        if (is_add){
            GameScreen.Instance.stage.addActor(this);
            this.toBack();
        }
        else
        {
            this.remove();
        }
    }

    void CreateDecks() {
        deck = new Group();
        this.addActor(deck);
        arrCardViews = new CardView[52];
        CreateArrayCardView();
    }

    public void NewGame(boolean is_resume) {
        AddToStage(true);
        if (!UIController.Instance.CheckTienCuoc())
        {
            OverlayUI.Show();
            UIController.Instance.optionUI.Show();
            UIController.Instance.optionUI.txtContent.setVisible(true);
            UIController.Instance.optionUI.txtContent.setText("Bạn cần "+Utilities.ShortCutMoney(GameController.GetScoreBet()*10)+" để chơi mức cược này!");
            return;
        }
        //reset
        ResetGame();
        //set score bet
        UIController.Instance.playingUI.SetScoreBet();
        //Set list info
        SetListInfo();
        //reset button
        ResetButton();
        //init Game
        InitGame(is_resume);
        //update info
        UpdateInfo();
    }

    void SetListPlayerViews() {
        //create ListPlayerView
        CreateListPlayerView();

        //SetPositionPlayerView
        SetPositionListPlayerView();
        playerViewsTemp = Arrays.copyOf(playerViews, playerViews.length);
    }

    void SetListInfo() {
        if (listPlayerInfo != null && listPlayerInfo.size == PreferenceController.Instance.preferences.getInteger("NumberPlayer"))
            return;
        //create list player info
        CreateListInfo();
        SetSwapPlayers();
    }

    private void CreateListInfo() {
        listPlayerInfo = GetListPlayerWillPlay();
        listInfoModel = new Array<InfoModel>();
        for (int i = 0; i < listPlayerInfo.size; i++) {
            listInfoModel.add(new InfoModel(listPlayerInfo.get(i)));
        }
    }


    private void AddBackGround() {
        background = new Image(Assets.GetTexture(Assets.backgroundGamePlay));
        background.setSize(MauBinh.V_WIDTH, MauBinh.V_HEIGHT);
        background.setPosition(-MauBinh.V_WIDTH / 2, -MauBinh.V_HEIGHT / 2);

        this.addActor(background);
    }


    void CreateListPlayerView() {
        for (int i = 0; i < 4; i++) {
            playerViews[i] = new PlayerView(i);
                 this.addActor(playerViews[i]);
            playerViews[i].toBack();
        }
        for (int i=0;i<4;i++)
            for (int j=0;j<3;j++)
                this.addActor(playerViews[i].listChi[j].typeName);
        background.toBack();
    }

    void SetPositionListPlayerView() {
        for (int i = 0; i < 4; i++) {
            SetPositionPlayerView(playerViews[i]);
        }
    }

    void SetPositionPlayerView(PlayerView playerView) {
        Vector2 phom_0_position=new Vector2(0,0);
        switch (playerView.index) {
            case 0:
                phom_0_position = new Vector2(0,-MauBinh.V_HEIGHT/2+60);
                playerView.listChi[0].setPosition(phom_0_position.x,phom_0_position.y);
                break;
            case 1:
                phom_0_position = new Vector2(MauBinh.V_WIDTH/2-350,-80);
                playerView.listChi[0].setPosition(phom_0_position.x,phom_0_position.y);
                break;
            case 2:
                phom_0_position = new Vector2(0,MauBinh.V_HEIGHT/2-200);
                playerView.listChi[0].setPosition(phom_0_position.x,phom_0_position.y);
                break;
            case 3:
                phom_0_position = new Vector2(-MauBinh.V_WIDTH/2+350,-80);
                playerView.listChi[0].setPosition(phom_0_position.x,phom_0_position.y);
                break;
        }
        playerView.listChi[1].setPosition(phom_0_position.x,phom_0_position.y+playerView.listChi[0].width*0.8f);
        playerView.listChi[2].setPosition(phom_0_position.x,phom_0_position.y+playerView.listChi[0].width*0.8f*2);
        playerView.UpdateGroupListCardView();
        playerView.SetPositionInfoView();
        //playerView.imgtReport.setPosition(playerView.listChi[1].getX(Align.center),playerView.listChi[1].getY(Align.center),Align.center);
        playerView.imgtReport.setPosition(playerView.infoView.getX(),playerView.infoView.getY()-130,Align.bottom);

        for (int i=0;i<3;i++)
            playerView.listChi[i].typeName.setPosition(playerView.listChi[i].getX(),playerView.listChi[i].getY()-30,Align.left);
    }

    void CheckBotHetTien(){
        Array<Integer> arr = new Array<Integer>(listPlayerInfo);
        for (int i=1;i<gameModel.listPlayer.size;i++)
        {
            PlayerModel player=gameModel.listPlayer.get(i);
            if (player.infoModel.coin<gameModel.scoreBet*10)
            {
                Preferences preferences = PreferenceController.Instance.preferences;
                int _ran = (int) (10 + Math.random()*(30-10));
                preferences.putString("ScoreOfPlayer"+player.infoModel.index,String.valueOf((long)(_ran*1000000)));
                preferences.flush();
                Random random = new Random();
                int next_ID;
                do {
                    next_ID = 1 + random.nextInt(13 - 1);
                } while (listPlayerInfo.contains(next_ID,false) && arr.contains(next_ID,false));
                arr.add(next_ID);
                player.infoModel.index=next_ID;
            }
        }
        listPlayerInfo.clear();
        for (int i=0;i<gameModel.listPlayer.size;i++){
            PlayerModel player = gameModel.listPlayer.get(i);
            player.Clear();
            listPlayerInfo.add(player.infoModel.index);
        }
    }

    void InitGame(boolean is_resume) {
        gameModel.Clear();
        gameModel.gameState= GameModel.GameState.START;
        if (!is_resume)
        {
            DataModel.Instance.Reset();
            DataModel.Instance.SaveListCardOnTable();
        }
        if (is_resume)
        {
            gameModel.listCardOnTable.clear();
            Utilities.ImportDataCards(PreferenceController.Instance.preferences.getString("ListCardOnTable"),gameModel.listCardOnTable);
        }
        gameModel.RegisterEndDistributeCards(new GameModel.OnEndDistributeCards() {
            @Override
            public void OnEnvent() {
                OnEndDistributeCards();
            }
        });
        gameModel.RegisterCompareBranch(new GameModel.OnCompareBranch() {
            @Override
            public void OnEvent(final int i) {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        UIController.Instance.playingUI.SetCountScore(Assets.GetTexture(Assets.GetChi(i+1)));
                    }
                });
            }
        });
        gameModel.RegisterShowSummaryScore(new GameModel.OnShowSummaryScore() {
            @Override
            public void OnEvent() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        UIController.Instance.playingUI.SetCountScore(Assets.GetTexture(Assets.img_ketthuc));
                        for (PlayerView playerView:playerViews)
                            playerView.infoView.SetMauBinh(false);
                    }
                });
            }
        });
        gameModel.ResgisterSetWinAll(new GameModel.OnSetWinAll() {
            @Override
            public void OnEvent() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                       // UIController.Instance.playingUI.SetCountScore(Assets.GetTexture(Assets.img_batsap3lang));
                        ShowEffect(Assets.GetTexture(Assets.img_batsap3lang));
                    }
                });
            }
        });
        gameModel.RegisterCountAce(new GameModel.OnCountAce() {
            @Override
            public void OnEvent() {
                Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        UIController.Instance.playingUI.SetCountScore(Assets.GetTexture(Assets.img_tinhat));
                    }
                });
            }
        });


        for (int i = 0; i < gameModel.listPlayer.size; i++) {
            final PlayerView playerView = playerViews[i];
            final PlayerModel playerModel = gameModel.listPlayer.get(i);
            playerModel.infoModel = listInfoModel.get(i);
            playerModel.RegisterSetUnChildToSort(new PlayerModel.OnSetUnChildToSort() {
                @Override
                public void OnEvent(final int index_list) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            playerView.SetUnChildToSort(index_list);
                        }
                    });
                }
            });
            playerModel.RegisterSortList(new PlayerModel.OnSortList() {
                @Override
                public void OnEvent(final int index_list) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            SortList(playerView, index_list);
                        }
                    });
                }
            });
            playerModel.RegisterSetInfo(new PlayerModel.OnSetInfo() {
                @Override
                public void OnEvent() {
                    {
                        if (playerModel.index==0)
                        {
                            UIController.Instance.startUI.SetCoin();
                        }
                        playerView.infoView.UpdateInfo(playerModel.infoModel);
                    }
                }
            });
            playerModel.RegisterChangeScore(new PlayerModel.OnChangeScore() {
                @Override
                public void OnEvent(final int score) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            OnChangeScore(playerView,score);
                        }
                    });
                }
            });
            playerModel.RegisterSetTypeBranch(new PlayerModel.OnSetTypeBranch() {
                @Override
                public void OnEvent(final int index_list, final GameModel.TypeBranch type_branch, final boolean isSpecial, final int number_1) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            String s="";
                            if (!isSpecial)
                                s=GameModel.Instance.GetNameOfTypeBranch(type_branch);
                            else
                                s=GameModel.Instance.GetNameOfTypeBranch(type_branch,index_list,number_1);

                            Label txtTypeName=playerView.listChi[index_list].typeName;
                            if(isSpecial){
                               if (gameModel.CheckBranchSpecial(type_branch,index_list,number_1))
                               {
                                   Label.LabelStyle style= new Label.LabelStyle(Assets.GetBitmapFont(Assets.font_purple), Color.WHITE);
                                   txtTypeName.setStyle(style);
                               }
                               else{
                                   Label.LabelStyle style= new Label.LabelStyle(Assets.GetBitmapFont(Assets.font_blue), Color.WHITE);
                                   txtTypeName.setStyle(style);
                               }
                            }
                            txtTypeName.setText(s);
                            //txtTypeName.setColor(GameSetting.Instance.arrColorTypeBranch[type_branch.ordinal()]);
                        }
                    });
                }
            });
            playerModel.RegisterHaveWinAbsolute(new PlayerModel.OnHaveWinAbsolute() {
                @Override
                public void OnEvent(GameModel.TypeWinAbsolute type) {
                    if (type == GameModel.TypeWinAbsolute.RONGLOC || type == GameModel.TypeWinAbsolute.SANHRONG || type == GameModel.TypeWinAbsolute.CUNGMAU13 || type == GameModel.TypeWinAbsolute.CUNGMAU12)
                    {
                        gameModel.listPlayer .get(0).isCompletedSort=true;
                        UIController.Instance.sortCardsUI.OnEndSort();
                    }
                    else
                    {
                        UIController.Instance.sortCardsUI.btnMauBinh.setVisible(true);
                    }
                }
            });
            playerModel.RegisterReportLungBai(new PlayerModel.OnReportLungBai() {
                @Override
                public void OnEvent() {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            playerView.SetReport(Assets.GetTexture(Assets.img_binhlung));
                        }
                    });
                }
            });
            playerModel.RegisterReportSap3Chi(new PlayerModel.OnReportSap3Chi() {
                @Override
                public void OnEvent() {
                    Gdx.app.postRunnable(new Runnable() {
                    @Override
                    public void run() {
                        playerView.SetReport(Assets.GetTexture(Assets.img_sap3chi));
                    }
                });
                }
            });
            playerModel.RegisterReportWinAbsolute(new PlayerModel.OnReportWinAbsolute() {
                @Override
                public void OnEvent(final GameModel.TypeWinAbsolute type) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            playerView.infoView.SetMauBinh(true);
                            ShowEffect(GetTextureAnTrang(type));
                        }
                    });
                }
            });
            playerModel.RegisterSetLockTypeBranch(new PlayerModel.OnLockTypeBranch() {
                @Override
                public void OnEvent() {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            for (int j=0;j<3;j++)
                                playerView.listChi[j].typeName.setVisible(false);
                        }
                    });
                }
            });
            playerModel.RegisterShowTypeBranch(new PlayerModel.OnShowTypeBranch() {
                @Override
                public void OnEvent(final int i) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            playerView.listChi[i].typeName.setVisible(true);
                        }
                    });
                }
            });
            playerModel.RegisterSetWinOrLose(new PlayerModel.OnWinOrLose() {
                @Override
                public void OnEvent(final int score) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            if (score>0)
                            {
                                playerView.SetReport(Assets.GetTexture(Assets.img_thang));
                            }
                            else
                            if (score==0)
                            {
                                playerView.SetReport(Assets.GetTexture(Assets.img_hue));
                            }
                            else
                            {
                                playerView.SetReport(Assets.GetTexture(Assets.img_thua));
                            }
                        }
                    });
                }
            });

        }

        for (int i = 0; i < gameModel.listCardOnTable.size; i++) {
            final CardView cardView = arrCardViews[i];
            deck.addActor(cardView);
            cardView.image.setPosition(0.7f * i, 1.2f * i, Align.center);

            final CardModel card_model = gameModel.listCardOnTable.get(i);
            card_model.RegisterOnSetParent(new CardModel.OnSetParent() {
                @Override
                public void OnEvent(final int index_player, final int index_list) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            playerViews[index_player].GetListByIndex(index_list).AddCardView(cardView);
                        }
                    });
                }
            });
            card_model.RegisterSetVisible(new CardModel.OnSetVisible() {
                @Override
                public void OnEvent(final boolean is_visible) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            cardView.SetVisible(is_visible, card_model);
                        }
                    });
                }
            });
            card_model.RegisterSetFlip(new CardModel.OnSetFlip() {
                @Override
                public void OnEvent(final int index) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            cardView.SetFlip(card_model, index);
                        }
                    });
                }
            });
            card_model.RegisterSetFirstDistribute(new CardModel.OnSetFirstDistribute() {
                @Override
                public void OnEvent() {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            cardView.SetFirstDistribute();
                        }
                    });
                }
            });
            card_model.RegisterSetShadow(new CardModel.OnSetShadow() {
                @Override
                public void OnEvent(final int level) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            cardView.SetShadow(level);
                        }
                    });
                }
            });
            card_model.RegisterSetCanClick(new CardModel.OnSetCanClick() {
                @Override
                public void OnEnvent(final boolean can_click) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            cardView.SetCanClick(can_click);
                        }
                    });
                }
            });
            card_model.RegisterSetPriority(new CardModel.OnPriority() {
                @Override
                public void OnEvent(boolean b) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            deck.addActor(cardView);
                        }
                    });
                }
            });
            card_model.RegisterRemoveActor(new CardModel.OnRemoveActor() {
                @Override
                public void OnEvent(final boolean is_immediately) {
                    Gdx.app.postRunnable(new Runnable() {
                        @Override
                        public void run() {
                            if (is_immediately)
                                deck.removeActor(cardView);
                            else
                            {
                                cardView.image.clearActions();
                                cardView.image.addAction(Actions.sequence(Actions.moveBy(0,1000,0.5f, Interpolation.swingIn),Actions.run(new Runnable() {
                                    @Override
                                    public void run() {
                                        deck.removeActor(cardView);
                                    }
                                })));
                            }
                        }
                    });
                }
            });
        }
        CheckBotHetTien();
        if (!is_resume)
            gameModel.DistributeCardsTest();
        else
        {
            DataModel.Instance.LoadData();
            for(CardModel card:gameModel.listCardOnTable)
            {
                card.SetRemoveActor(false);
            }
            switch (gameModel.playState)
            {
                case MAYXEPBAI:
                    OnEndDistributeCards();
                    break;
                case PLAYERXEPBAI:
                    OnPlayerXepBai();
                    break;
                case CHUANHANBAI:
                    gameModel.DistributeCardsTest();
                    break;
                case TINHTIEN:
                    gameModel.CountScore();
                    break;
                case TINHTIENXONG:
                    Gdx.app.log("End game","endgame");
                    break;
                default:
                    break;
            }
        }
    }



    void SortList(PlayerView playerView,int index_list){
        ListCardView listCardView=playerView.GetListByIndex(index_list);
        listCardView.SortCenter(0.7f);
    }
    void OnEndDistributeCards(){
        for(CardModel card:gameModel.listCardOnTable)
        {
            card.SetRemoveActor(false);
        }
        MoveCardsAfterDistribute();
        SetViewListCards();
        OpenCardsMainPlayer();
       // for (PlayerModel playerModel:gameModel.listPlayer)
        //    playerModel.OpenAllList();
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                UIController.Instance.machineSortingUI.Show();
            }
        },0.8f);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                for ( PlayerModel player : gameModel.listPlayer) {
                    if (player.index==0)
                        continue;
                    gameModel.SortAIForPlayer(player);
                }
                UIController.Instance.machineSortingUI.Hide();
//                GameModel.Instance.SortAIForMainPlayer();
                UIController.Instance.playingUI.onClickXepBai();
                SaveDataAfterMayXepBai();
            }
        },1f);
        SaveDataAfterDistribute();
    }
    void OnPlayerXepBai(){
        for (int i=1;i<gameModel.listPlayer.size;i++)
            gameModel.listPlayer.get(i).isCompletedSort=true;
        for(CardModel card:gameModel.listCardOnTable)
        {
            card.SetRemoveActor(false);
        }
        MoveCardsAfterDistribute();
        SetViewListCards();
        OpenCardsMainPlayer();
        UIController.Instance.playingUI.onClickXepBai();
    }
    void MoveCardsAfterDistribute(){
        for (CardView cardView:arrCardViews)
        {
            cardView.image.setOrigin(Align.center);
            cardView.image.addAction(Actions.rotateTo(0,0.2f));
        }
        for (PlayerView playerView:playerViews)
            for (ListCardView listCardView:playerView.listChi)
                listCardView.MoveToParentCenter(playerView.listChi[1]);
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                SoundController.Instance.PlayClipDownCards();
            }
        },0.5f);
    }
    void SetViewListCards(){

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                if (!gameModel.isPlaying)
                    return;
                for (int i=0;i<gameModel.listPlayer.size;i++)
                    gameModel.listPlayer.get(i).SetUpdateListHold();
            }
        },0.5f);
    }
    void SaveDataAfterDistribute(){
        //save data
        gameModel.gameState= GameModel.GameState.START;
        gameModel.playState= GameModel.PlayState.MAYXEPBAI;
        DataModel.Instance.SaveGameState();
        DataModel.Instance.SaveCards();
        DataModel.Instance.SavePlayState();
        DataModel.Instance.SaveListHost();
    }
    void SaveDataAfterMayXepBai(){
        //save data
        gameModel.gameState= GameModel.GameState.START;
        gameModel.playState= GameModel.PlayState.PLAYERXEPBAI;
        DataModel.Instance.SaveGameState();
        DataModel.Instance.SaveCards();
        DataModel.Instance.SavePlayState();
        DataModel.Instance.SaveListHost();
    }
    void OpenCardsMainPlayer(){
        gameModel.GetMainPlayer().OpenAllList();
    }

    void UpdateInfo(){
        for (int i=0;i<gameModel.listPlayer.size;i++)
            gameModel.listPlayer.get(i).SetInfo();
    }

    Array<Integer> GetListPlayerWillPlay() {
        Array<Integer> list = new Array<Integer>();
        int number_player = PreferenceController.Instance.preferences.getInteger("NumberPlayer");
        int ran;
        Random random = new Random();
        list.add(0);
        while (list.size < number_player) {
            do {
                ran = 1 + random.nextInt(13 - 1);
            } while (list.contains(ran, false));
            list.add(ran);
        }
        return list;
    }
    public void ResetGame(){
        currentChooseCard=null;
        listChooseCard.clear();
        for (CardView cardView:arrCardViews)
        {
            cardView.Reset();
        }
        for (PlayerView playerView:playerViews)
            playerView.Clear();
        gameModel.SetDestroy ();
        ResetButton();
        ResetListQuanLy(listQuanLyScore);
        ResetListQuanLy(listQuanLyRank);
        ResetListQuanLy(listQuanLyArrow);
        UIController.Instance.fireWorkUI.HideFireWork();
        UIController.Instance.playingUI.SetCountScore(false);
        UIController.Instance.playingUI.btnXemLai.setVisible(false);
    }

    void ResetButton(){
        PlayingUI playingUI = UIController.Instance.playingUI;
        playingUI.btnVanMoi.setVisible(false);
    }

    void CreateArrayCardView(){
        for (int i=0;i<arrCardViews.length;i++)
        {
            final CardView cardView = new CardView();
            deck.addActor(cardView);
            cardView.image.setPosition(deck.getX()-cardView.image.getWidth()/2,deck.getY()-cardView.image.getHeight()/2);
            arrCardViews[i]=cardView;
        }
    }
    void AddListQuanLy(Array<Actor> list, Actor actor){
        list.add(actor);
        this.addActor(actor);
    }
    void ResetListQuanLy(Array<Actor> list){
        for (Actor actor:list
                ) {
            removeActor(actor);
        }
        list.clear();
    }


    public static int GetScoreBet(){
        return UIController.Instance.ListScoreBet().get(PreferenceController.Instance.preferences.getInteger("ScoreBet"));
    }

    public void locateActors(){
        background.setSize(MauBinh.V_WIDTH,MauBinh.V_HEIGHT);
        background.setPosition(0,0,Align.center);
        for (int i = 0; i < 4; i++) {
            SetPositionPlayerView(playerViews[i]);
        }
    }
    public void DisableSubTime(){}
    public void OnCompleteSort()
    {
//		StartCoroutine (gameModel.CountScore());
        gameModel.GetMainPlayer().SetCompletedSort();
    }
    public void DisableSort()
    {
    }
    void OnChangeScore(PlayerView playerView,int score){
        final Label txtScore;
        if (score>=0)
        {
            txtScore = GameUI.NewLabel("",Assets.GetBitmapFont(Assets.font_gold));
        }
        else
        {
            txtScore = GameUI.NewLabel("",Assets.GetBitmapFont(Assets.font_silver));
        }
        txtScore.setWidth(0);
        txtScore.setAlignment(Align.center);
        GameUI.ScaleLabel(txtScore,0.8f);
        txtScore.setPosition(playerView.GetListByIndex(0).getX(),playerView.GetListByIndex(0).getY()+50,Align.center);
        String s="";
        if (score>=0)
            s+="+";
        else
        if (score<0)
            s+="-";
        s+=""+ Utilities.ShortCutMoney(Math.abs(score));

        String s2="";
        if (score>=0)
            s2+="(Ăn ";
        else
        if (score<0)
            s2+="(Thua ";
        s2+=""+ (int)(Math.abs(score)/gameModel.scoreBet);
        s2+=" Chi)";

        s+="\n"+s2;

        txtScore.setText(s);
        txtScore.addAction(Actions.moveBy(0,60,1f));


        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                removeActor(txtScore);
                listQuanLyScore.removeValue(txtScore,false);
            }
        },2f);

        AddListQuanLy(listQuanLyScore,txtScore);
    }
    public void HideTextCountScore(){
        UIController.Instance.playingUI.SetCountScore(false);
    }
    public void ClearReportPlayerViews(){
        for (int i=0;i<playerViews.length;i++)
        {
            playerViews[i].SetReport(false);
        }
    }
    public void ClearColorReportPlayerViews(){
//        for (int i=0;i<playerViews.length;i++)
//        {
//            playerViews[i].txtReport.setColor(Color.BLACK);
//        }
    }
    void ShowEffect(Texture texture){
        UIController.Instance.playingUI.effectActor.UpdateTexture(texture,2);
        UIController.Instance.playingUI.effectActor.UpdateTexture(texture,3);
        UIController.Instance.playingUI.ShowEffect();
        UIController.Instance.fireWorkUI.ShowFireWork();
        SoundController.Instance.PlayClipWin();
    }
    Texture GetTextureAnTrang(GameModel.TypeWinAbsolute type){
        switch (type)
        {
            case BASANH:
                return Assets.GetTexture(Assets.img3Sanh);
            case LUCPHE:
                return Assets.GetTexture(Assets.imgLucPhe);
            case BATHUNG:
                return Assets.GetTexture(Assets.img3Thung);
            case CUNGMAU12:
                return Assets.GetTexture(Assets.imgDongHoa);
            case CUNGMAU13:
                return Assets.GetTexture(Assets.imgDongHoa);
            case RONGLOC:
                return Assets.GetTexture(Assets.imgRongCuon);
            case SANHRONG:
                return Assets.GetTexture(Assets.imgSanhRong);
            default:
                Gdx.app.log("Error","Loi an trang");
                return Assets.GetTexture(Assets.overlay);
        }
    }

    void SetSwapPlayers(){
        playerViews= Arrays.copyOf(playerViewsTemp,playerViewsTemp.length);
        int countPlayer = PreferenceController.Instance.preferences.getInteger("NumberPlayer");
        switch (countPlayer)
        {
            case 2:
                SwapPlayerByIndex(1,2);
                break;
            case 3:
                SwapPlayerByIndex(2,3);
                break;
        }
        SetActivePlayer(countPlayer);
    }
    void SetActivePlayer(int countPlayer){
        for (int i = 0; i < playerViewsTemp.length; i++) {
            if (!IsPlayerExist(countPlayer,i)) {
                playerViewsTemp [i].setVisible(false);
            } else {
                playerViewsTemp [i].setVisible(true);
            }
        }
    }
    boolean IsPlayerExist(int countPlayer,int i_player){
        for (int i = 0; i < countPlayer; i++) {
            if (playerViewsTemp [i_player] == playerViews [i])
                return true;
        }
        return false;
    }
    void SwapPlayerByIndex(int index_1,int index_2)
    {
        playerViews[index_1]=playerViewsTemp[index_2];
    }
    public void OnEndGame(){
        if (gameModel.is_end)
            return;
        SetEmotion();
    }
    void SetEmotion(){
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                for (int i=0;i<gameModel.listPlayer.size;i++){
                    Texture texture;
                    int score=gameModel.listPlayer.get(i).scoreChange;
                    if (score==0)
                        continue;
                    if (score>0)
                        texture=Assets.GetTexture(Assets.GetSmile((int) (Math.random()*(1))));
                    else
                        texture=Assets.GetTexture(Assets.GetCry((int) (Math.random()*(1))));
                    playerViews[i].infoView.ShowEmoticon(texture);
                }
            }
        });
    }
    String GetNameTypeWinAbsolute(GameModel.TypeWinAbsolute type){
        switch(type)
        {
            case LUCPHE:
                return "LỤC PHÉ";
            case BASANH:
                return "BA SẢNH";
            case BATHUNG:
                return "BA THÙNG";
            case CUNGMAU12:
                return "ĐỒNG HOA";
            case CUNGMAU13:
                return "ĐỒNG HOA";
            case NAMDOIMOTXAM:
                return "NĂM ĐÔI MỘT XÁM";
            case RONGLOC:
                return "RỒNG LỐC";
            case SANHRONG:
                return "SẢNH RỒNG";
            default:
                return "";
        }
    }
}
