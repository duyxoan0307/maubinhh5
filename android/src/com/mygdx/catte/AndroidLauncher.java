package com.mygdx.catte;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.mygdx.catte.MauBinh;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		IZen iZen=new IZen() {
			@Override
			public void ShowFullscreen() {

			}

			@Override
			public void ShowBanner(boolean visible) {

			}

			@Override
			public void TrackLevelStart(int level) {

			}

			@Override
			public void TrackLevelFailed(int level) {

			}

			@Override
			public void TrackLevelCompleted(int level) {

			}

			@Override
			public void ShowLeaderBoard() {

			}

			@Override
			public void ReportScore(String leaderboardID, long score) {

			}

			@Override
			public void Rate() {

			}

			@Override
			public void Like() {

			}

			@Override
			public String GetDefaultLanguage() {
				return null;
			}

			@Override
			public boolean isVideoRewardReady() {
				return false;
			}

			@Override
			public void ShowVideoReward(OnVideoRewardClosed callback) {

			}

			@Override
			public void LinkOtherGame(String packageName) {

			}

			@Override
			public void LoadUrlTexture(String url, UrlTextureCallback callback) {

			}

			@Override
			public void FBInstant_GetDoubleStats(String statname, FBInstant_GetStatsCallback callback) {

			}

			@Override
			public void FBInstant_SetDoubleStats(String statname, double value, FBInstant_SetStatsCallback callback) {

			}

			@Override
			public void FBInstant_IncrementDoubleStats(String statname, double value, FBInstant_IncrementDoubleStatsCallback callback) {

			}

			@Override
			public void FBInstant_LoadLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_LoadMyLeaderboard(FBInstant_LeaderboardEntryCallback callback) {

			}

			@Override
			public void FBInstant_GetPlayerInfo(FBInstant_PlayerInfoCallback callback) {

			}

			@Override
			public void OnShow() {

			}
		};
		initialize(new MauBinh(iZen), config);
	}
}
